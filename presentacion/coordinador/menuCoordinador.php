<?php
$coordinador = new Coordinador($_SESSION["id"]);
$coordinador->consultar();
?>
<nav class="navbar navbar-expand-lg navbar-dark sticky-top bg-dark">
  	<a class="navbar-brand"
		href="index.php?pid=<?php echo base64_encode("presentacion/sesionCoordinador.php") ?>"><i
		class="fas fa-home"></i></a>
  <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navb" aria-expanded="true">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div id="navb" class="navbar-collapse collapse hide">
    <ul class="navbar-nav">
     <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Profesores
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
        <a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/profesor/registrarProfesor.php")?>">Nuevo Profesor</a> 
        <a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/profesor/consultarProfesorPagina.php")?>">consultar Profesores</a> 

        </div>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="index.php?pid=<?php echo base64_encode("presentacion/pregunta/crearPregunta.php") ?>">Cuestionario</a>
      </li>

      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Admisiones
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
        <a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/caso/consultarCasosPagina.php") ?>&filtro=cuestionario calificado">Admisiones</a> 
        <a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/caso/consultarCasosPagina.php") ?>&filtro=opcionado">Opcionados</a> 
        <a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/caso/consultarCasosPagina.php") ?>&filtro=admitido">Admitidos</a> 
        <a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/caso/consultarCasosPagina.php") ?>&filtro=rechazado">Rechazados</a> 
      </div>
      </li>

      <li class="nav-item">
        <a class="nav-link" href="index.php?pid=<?php echo base64_encode("presentacion/estadisticas.php") ?>">Graficos</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="index.php?pid=<?php echo base64_encode("presentacion/reportesPagina.php") ?>">Reportes</a>
      </li>
    
     
    </ul>

    <ul class="nav navbar-nav ml-auto">
    	<li class="nav-item dropdown"><a
				class="nav-link dropdown-toggle" href="#" id="navbarDropdown"
				role="button" data-toggle="dropdown" aria-haspopup="true"
				aria-expanded="false">Coordinador: <?php echo $coordinador -> getNombre() ?> <?php echo $coordinador -> getApellido() ?></a>
				<div class="dropdown-menu" aria-labelledby="navbarDropdown">
					<a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/administrador/editarClave.php")?>&idCoordinador=<?php echo $_SESSION["id"]?>" method="post">Cambiar Clave</a>
				</div>
		</li>
      <li class="nav-item">
        <a class="nav-link" href="index.php?cerrarSesion=true"><span class="fas fa-sign-in-alt"></span> Cerrar sesion</a>
      </li>
    </ul>
  </div>
</nav>
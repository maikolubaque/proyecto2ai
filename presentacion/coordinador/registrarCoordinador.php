<?php 
$nombre = "";
$apellido = "";
$correo = "";
$clave = "";
$clave2 = "";
$carreraId = "";
$carrera = new Carrera();
$carreras = $carrera -> consultarTodos();


if (isset($_POST["registrar"])) {
    $nombre = $_POST["nombre"];
    $apellido = $_POST["apellido"];
    $correo = $_POST["correo"];
    $carreraNombre = $_POST["carrera"];
    $clave = $_POST["clave"];
    $clave2 = $_POST["clave2"];
	$error = "";
	foreach($carreras as $c){
		if($c -> getCarrera() == $carreraNombre){
			$carreraId = $c -> getIdCarrera();
		}
	}

    $coordinador = new Coordinador("",$nombre, $apellido,$correo,$clave, $carreraId, 1);
    if(!$coordinador  ->existeCorreo())
    {
        if($clave==$clave2)
        {
            $coordinador  ->registrar();
            
            /*if ($_FILES["imagen"]["name"] != "") {
                $rutaLocal = $_FILES["imagen"]["tmp_name"];
                $tipo_imagen = $_FILES["imagen"]["type"];
                $tiempo = new DateTime();
                $rutaRemota = "Imagenes_Coordinadors/" . $tiempo->getTimestamp() . (($tipo_imagen == "image/png") ? ".png" : ".jpg");
                copy($rutaLocal, $rutaRemota);
                $coordinador ->consultar();
                if($coordinador  -> getFoto() != ""){
                    unlink($coordinador -> getFoto());
                }
                $coordinador = new  Coordinador("", $nombre, $apellido, $correo, $clave,$rutaRemota,"");
                $coordinador  ->registrar();
            } else {
                $coordinador ->registrar();
			}
			$datosLog = "Nombre: ".$nombre."; Apellido: ".$apellido."; Correo: ".$correo;
			$log = new Log("", "Registro coordinador", $datosLog, date("yy-m-d"), date("g:i a"), $_SESSION["userName"]);
			$log -> insertar();*/
        }else {
            $error=1;
        }
    }else {
        $error=2;
       
    }
}


?>
<div class="container mt-3">
	<div class="row">
		<div class="col-lg-3 col-md-0"></div>
		<div class="col-lg-6 col-md-12">
            <div class="card">
				<div class="card-header text-white bg-dark">
					<h4>Registrar Coordinador</h4>
				</div>
              	<div class="card-body">
					<?php if(isset($_POST["registrar"])){ 
					if($error==1)
					{
					    echo '<div class="alert alert-danger alert-dismissible fade show" role="alert">';
					    echo 'La clave no coincide';
					    echo '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
					    echo '</div>';
					}else if($error ==2)
					{
					    echo '<div class="alert alert-danger alert-dismissible fade show" role="alert">';
					    echo 'el correo ya existe';
					    echo '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
					    echo '</div>';
					}else {
					?>
					<div class="alert alert-success alert-dismissible fade show" role="alert">
						Coordinador de carrera registrado con exito.
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					</div>
					<?php }} ?>
					<form action="index.php?pid=<?php echo base64_encode("presentacion/coordinador/registrarCoordinador.php")?>" method="post" enctype="multipart/form-data">
						<div class="form-group">
							<label>Nombre</label> 
							<input type="text" name="nombre" class="form-control"  required>
						</div>
						<div class="form-group">
							<label>Apellido</label> 
							<input type="text" name="apellido" class="form-control"  required>
						</div>
						<div class="form-group">
							<label>Correo</label> 
							<input type="email" name="correo" class="form-control"  required>
						</div>
						<div class="form-group">
							<label>Clave</label> 
							<input type="password" name="clave" class="form-control"  required>
						</div>
						<div class="form-group">
							<label>Confirmar clave</label> 
							<input type="password" name="clave2" class="form-control"  required>
						</div>
						<div class="form-group">
                            <label for="Carrera">Carrera</label><br>
                            <select id="select-tipo" name="carrera">
                                <?php
                                foreach ($carreras as $cActual) {
                                    echo "<option>";
                                    echo "<a class='dropdown-item' href='#'>" . $cActual->getCarrera() . "</a>";
                                    echo "</option>";
                                }
                                ?>
                            </select>
                        </div>
						 
						<button type="submit" name="registrar" class="btn btn-dark">Registrar</button>
					</form>
            	</div>
            </div>
		</div>
	</div>
</div>
<?php
$carrera = new Carrera();
$carreras = $carrera -> consultarTodos();
if(isset($_POST["modificar"])){
    $nombre = $_POST["nombre"];
    $apellido = $_POST["apellido"];
    $correo = $_POST["correo"];
    $carreraNombre  = $_POST["carrera"];
    foreach($carreras as $c){
		if($c -> getCarrera() == $carreraNombre){
			$carreraId = $c -> getIdCarrera();
		}
    }
    
    $coordinador = new Coordinador($_GET["idCoordinador"],$nombre, $apellido,$correo,"", $carreraId, 1);
    $coordinador -> editar();
   
   
/*	$userLog = "";
	if ($_SESSION["rol"] == "Coordinador")
		$userLog = $nombre . " " . $apellido;
	else
		$userLog = $_SESSION["userName"];
    if($_SESSION["rol"]=="Administrador")
	   $datosLog = "Nombre: ".$nombre."; Apellido: ".$apellido."; Correo: ".$correo."; estado:".$estado;
    else 
        $datosLog = "Nombre: ".$nombre."; Apellido: ".$apellido."; Correo: ".$correo;
	$log = new Log("", "Modificar coordinador", $datosLog, date("yy-m-d"), date("g:i a"), $userLog);
	$log -> insertar();*/
}else{
    $coordinador = new Coordinador($_GET["idCoordinador"]);
    $coordinador-> consultar();
}
?>
<div class="container mt-3">
	<div class="row">
		<div class="col-lg-3 col-md-0"></div>
		<div class="col-lg-6 col-md-12">
            <div class="card">
				<div class="card-header text-white bg-dark">
					<h4>Editar Coordinador</h4>
				</div>
              	<div class="card-body">
					<?php if(isset($_POST["modificar"])){ ?>
					<div class="alert alert-success alert-dismissible fade show" role="alert">
						Datos editados
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					</div>
					<?php } ?>
					<form action="index.php?pid=<?php echo base64_encode("presentacion/coordinador/modificarCoordinador.php") ?>&idCoordinador=<?php echo $_GET["idCoordinador"]?>" method="post" enctype="multipart/form-data">
						<div class="form-group">
							<label>Nombre</label> 
							<input type="text" name="nombre" class="form-control" value="<?php echo $coordinador -> getNombre() ?>" required>
						</div>
						<div class="form-group">
							<label>Apellido</label> 
							<input type="text" name="apellido" class="form-control" min="1" value="<?php echo $coordinador -> getApellido() ?>" required>
						</div>
						<div class="form-group">
							<label>Correo</label> 
							<input type="email" name="correo" class="form-control" min="1" value="<?php echo $coordinador -> getCorreo() ?>" required>
						</div>
						<div class="form-group">
                            <label for="Carrera">Carrera</label><br>
                            <select id="select-tipo" name="carrera">
                                <?php
                                echo "<option>";
                                echo ($coordinador->getCarrera()>0)?"":"<a class='dropdown-item' href='#'>" . $coordinador->getCarrera() . "</a>";
                                echo "</option>";
                                foreach ($carreras as $cActual) {
                                    echo "<option>";
                                    echo "<a class='dropdown-item' href='#'>" . $cActual->getCarrera() . "</a>";
                                    echo "</option>";
                                }
                                ?>
                            </select>
                        </div>
						
						
						<button type="submit" name="modificar" class="btn btn-dark">Modificar</button>
					</form>
            	</div>
            </div>
		</div>
	</div>
</div>
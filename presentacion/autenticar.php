
<?php
$correo = $_POST["correo"];
$clave = $_POST["clave"];
$administrador = new Administrador("", "", "", $correo, $clave);
$profesor = new Profesor("", "", "", $correo, $clave);
$coordinador = new Coordinador("", "", "", $correo, $clave);
$aspirante = new Aspirante("", "", "","","","", $correo, $clave);

if($administrador -> autenticar()){
    $_SESSION["id"] = $administrador -> getIdAdministrador();
    $_SESSION["rol"] = "Administrador";
    $_SESSION["userName"] = $administrador -> getNombre()." ".$administrador -> getApellido();
    $log = new Log("", "Ingreso al sistema", "Rol: Administrador", date("yy-m-d"), date("g:i a"), $administrador -> getNombre()." ".$administrador -> getApellido());
    $log -> insertar();
    header("Location: index.php?pid=" . base64_encode("presentacion/sesionAdministrador.php"));
}else if($profesor -> autenticar()){
       if($profesor -> getEstado() == -1){
        header("Location: index.php?pid=" . base64_encode("presentacion/inicio.php")."&error=4");
    }else if($profesor -> getEstado() == 0){
        header("Location: index.php?pid=" . base64_encode("presentacion/inicio.php")."&error=3");
    }else{
        $_SESSION["id"] = $profesor -> getIdprofesor();
        $_SESSION["rol"] = "Profesor";
        $_SESSION["userName"] = $profesor -> getNombre()." ".$profesor -> getApellido();
        header("Location: index.php?pid=" . base64_encode("presentacion/sesionProfesor.php"));
        $log = new Log("", "Ingreso al sistema", "Rol: profesor", date("yy-m-d"), date("g:i a"), $profesor -> getNombre()." ".$profesor -> getApellido());
        $log -> insertar();
    }
}else if($coordinador -> autenticar()){
    if($coordinador -> getEstado() == -1){
        header("Location: index.php?pid=" . base64_encode("presentacion/inicio.php")."&error=4");
    }else if($coordinador -> getEstado() == 0){
        header("Location: index.php?pid=" . base64_encode("presentacion/inicio.php")."&error=3");
    }else{
        $_SESSION["id"] = $coordinador -> getIdCoordinador();
        $_SESSION["rol"] = "Coordinador";
        $_SESSION["userName"] = $coordinador -> getNombre()." ".$coordinador -> getApellido();
        header("Location: index.php?pid=" . base64_encode("presentacion/sesionCoordinador.php"));
        $log = new Log("", "Ingreso al sistema", "Rol: Coordinador", date("yy-m-d"), date("g:i a"), $coordinador -> getNombre()." ".$coordinador -> getApellido());
        $log -> insertar();
    }
}else if($aspirante -> autenticar()){
    if($aspirante -> getEstado() == -1){
        header("Location: index.php?pid=" . base64_encode("presentacion/inicio.php")."&error=4");
    }else if($aspirante -> getEstado() == 0){
        header("Location: index.php?pid=" . base64_encode("presentacion/inicio.php")."&error=3");
    }else{
        $_SESSION["id"] = $aspirante -> getIdAspirante();
        $_SESSION["rol"] = "Aspirante";
        $_SESSION["userName"] = $aspirante -> getNombre()." ".$aspirante -> getApellido();
        header("Location: index.php?pid=" . base64_encode("presentacion/sesionAspirante.php"));
        $log = new Log("", "Ingreso al sistema", "Rol: Aspirante", date("yy-m-d"), date("g:i a"), $coordinador -> getNombre()." ".$coordinador -> getApellido());
        $log -> insertar();
    }
}else{
    $_SESSION["id"]="";
     header("Location: index.php?pid=" . base64_encode("presentacion/inicio.php")."&error=2");
}

?>
<?php
$caso = new Caso("", "", "", $_SESSION["id"]);
$caso->consultar();
$carrera = $caso->getCarrera();
$pregunta = new Pregunta("", "", $carrera);
$preguntas = $pregunta->consultarTodos();

if (isset($_POST["registrar"])) {
    $casoId = $caso -> getIdCaso();
    $fecha = date("yy-m-d H:i ");
    $entrevista = new Entrevista("", "" ,$fecha, "respondida", "", $casoId);
    $idEntrevista = $entrevista -> registrar(); 

    foreach($preguntas as $pr){
        $e_p = new Entrevista_Pregunta("", $pr -> getPregunta(), $_POST[$pr -> getIdPregunta()], "", $idEntrevista);
        $e_p -> insertar();
    }

    $caso -> modificarEstadoFecha("entrevista", $fecha);

    header("Location: index.php?pid=" . base64_encode("presentacion/sesionAspirante.php"));
}

?>

<div class="container mt-2">
    <div class="row mt-2 justify-content-lg-center">
        <div class="col-lg-8">
            <div class="card">
                <div class="card-header">
                    <h4>Entrevista - Cuestionario</h4>
                </div>
                <div class="card-body">
                    <?php if (isset($_POST["registrar"])) {
                    ?>
                        <div class="text-center">
                            <div class="spinner-border text-info" role="status">
                                <span class="sr-only">Cargando...</span>
                            </div>
                        </div>
                    <?php } ?>
                    <h5>Preguntas</h5>
                    <span style="font-size: 14px;">todas las preguntas son de tipo abiertas y todas se deben responder.</span>
                    <hr>
                    <form action="index.php?pid=<?php echo base64_encode("presentacion/proceso/entrevista.php") ?>" method="post" enctype="multipart/form-data">

                        <?php
                        $i = 1;
                        foreach ($preguntas as $p) {
                            echo "<div class='form-group'>";
                            echo "<label>" . $i . ". " . $p->getPregunta() . "</label>";
                            echo "<textarea type='text' name=" . $p->getIdPregunta() . " class='form-control' required></textarea>";
                            echo "</div>";
                            $i++;
                        }
                        ?>

                        <button type="submit" name="registrar" class="btn btn-dark">Registrar</button>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
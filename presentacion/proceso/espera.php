<?php
$caso = new Caso("","","",$_SESSION["id"]);
$caso -> consultar();

?>
<div class="container mt-2">
    <div class="row justify-content-lg-center">
        <div class="col-lg-8">
            <div class="card">
                <div class="card-header">
                    <h4>En espera</h4>
                </div>
                <div class="card-body">
                    <?php if($caso -> getEstado() == "cuestionario calificado" ||$caso -> getEstado() == "entrevista"){?>
                    <div>
                        <strong>Descripción: </strong> El proceso de aplicación se ha completado. Su estado es: en revision.
                        proximamente podrá conocer el resultado de la admisión. 
                    </div></br>

                    <?php 
                        }else if($caso -> getEstado() == "admitido"){echo "<strong class='text-success'>Usted ha sido admitido en la Universidad...</strong>";
                        }else if($caso -> getEstado() == "opcionado"){ echo "<strong class='text-warning'>Uted ha sido opcionado, por favor revise despues su nuevo estado</strong>";
                        }else if($caso -> getEstado() == "rechazado"){ echo "<strong class='text-danger'>Usted ha sido rechazado</strong>";} ?>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container mt-2">
    <div class="row justify-content-lg-center">
        <div class="col-lg-8">
            <div class="card">
                <div class="card-header">
                    <h4>Cuestionario</h4>
                </div>
                <div class="card-body">
                    <div>
                        <strong>Descripción: </strong> El cargue de información y documentación a la plataforma se 
                        ha completado satisfactoriamente, a continuación deberá responder el cuestionario de ingreso el cual
                        sera evaluado por un docente de la universidad.
                    </div></br>

                    <a href="index.php?pid=<?php echo base64_encode("presentacion/proceso/entrevista.php") ?>"><button type="button" class="btn btn-info">Continuar</button></a>
                </div>
            </div>
        </div>
    </div>
</div>
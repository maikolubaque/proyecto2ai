<?php
$caso = new Caso($_GET["idCaso"]);
$caso->consultar3();

$carrera = new Carrera($caso->getCarrera());
$carrera->consultar();

$aspirante = new Aspirante($caso->getAspirante());
$aspirante->consultar();

$residencia = new Residencia("", "", "", "", "", "", $aspirante->getIdAspirante());
$residencia->consultar();

$entrevista = new Entrevista("", "", "", "", "", $caso->getIdCaso());
$entrevista->consultar2();

$academia = new Academia("", "", "", "", "",  $aspirante->getIdAspirante());
$academia->consultar();

if (isset($_GET["accion"])) {
    $accion = $_GET["accion"];
    $fecha = date("yy-m-d g:i ");
    if ($accion == "1") {
        $caso->modificarEstadoFechaCaso("admitido", $fecha);
    } else if ($accion == "2") {
        $caso->modificarEstadoFechaCaso("opcionado", $fecha);
    } else if ($accion == "3") {
        $caso->modificarEstadoFechaCaso("rechazado", $fecha);
    }

    $caso->consultar3();
}

?>

<div class="container">
    <div class="text-center">
        <h2>Admisión</h2>
    </div>
    <div class="row">
        <div class="col-md-6">

            <hr>
            <div class="text-center">
                <h4>Información de la persona</h4>
            </div>
            <strong>Nombres: </strong><span><?php echo $aspirante->getNombre(); ?></span>
            <strong>Apellidos: </strong><span><?php echo $aspirante->getApellido(); ?></span><br>
            <strong>Documento de indentidad: </strong><span><?php echo $aspirante->getCedula(); ?></span><br>
            <strong>Sexo Biologico: </strong><span><?php echo $aspirante->getSexo(); ?></span><br>
            <strong>Telefono: </strong><span><?php echo $aspirante->getTelefono(); ?></span><br>
            <strong>Correo: </strong><span><?php echo $aspirante->getCorreo(); ?></span><br>

        </div>
        <div class="col-md-6">
            <hr>
            <div class="text-center">
                <h4>Información de residencia</h4>
            </div>
            <strong>Direccion: </strong><span><?php echo $residencia->getDireccion(); ?></span><br>
            <strong>Localidad: </strong><span><?php echo $residencia->getLocalidad(); ?></span><br>
            <strong>Ciudad: </strong><span><?php echo $residencia->getCiudad(); ?></span><br>
            <strong>Estrado de la vivienda: </strong><span><?php echo $residencia->getEstrato(); ?></span><br>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <hr>
            <div class="text-center">
                <h4>Información Academica</h4>
            </div>
            <strong>Colegio de grado: </strong><span><?php echo $academia->getColegio(); ?></span><br>
            <strong>Fecha grado: </strong><span><?php echo $academia->getFechaGrado(); ?></span><br>
            <strong>Puntaje ICFES: </strong><span><?php echo $academia->getIcfes(); ?></span><br>
        </div>
    </div>

    <div class="row">
        <div class="col">
            <hr>
            <div class="text-center">
                <h4>Información de la aplicación</h4>
            </div>
            <strong>Carrera a aplicar: </strong><span><?php echo $carrera->getCarrera(); ?></span><br>
            <strong>Puntaje de cuestionario: </strong><span><?php echo $entrevista->getPuntaje(); ?></span><br>
            <strong>Puntaje de cuestionario Ponderado (30% de 100): </strong><span><?php echo $entrevista->getPuntaje() * 30 / 50; ?></span><br>
            <strong>Puntaje ICFES saber 11: </strong><span><?php echo $academia->getIcfes() ?></span><br>
            <strong>Puntaje ICFES saber 11 ponderado (70% de 100): </strong><span><?php echo $academia->getIcfes() * 70 / 500; ?></span><br>
            <strong>Puntaje total ponderado: </strong><span><?php echo ($academia->getIcfes() * 70 / 500) + ($entrevista->getPuntaje() * 30 / 50); ?></span><br>
            <!-- <strong>Asignar puntaje adicional (de 0 10, 10% de 100): </strong>  -->
        </div>
    </div>
    <div class="row">
        <div class="col">
            <hr>
            <div class="text-center">
                <h4>Admisión</h4>
            </div>
            <?php if ($caso->getEstado() == "cuestionario calificado") { ?>
                <span>Esta acción es irreversible</span><br>
                <strong>accion:</strong><br>
                <a href="index.php?pid=<?php echo base64_encode("presentacion/proceso/admision.php") . "&idCaso=" . $_GET["idCaso"] . "&accion=3" ?>"><button class="btn-danger btn">Rechazar</button></a>
                <a href="index.php?pid=<?php echo base64_encode("presentacion/proceso/admision.php") . "&idCaso=" . $_GET["idCaso"] . "&accion=2" ?>"><button class="btn-warning btn">Opcionado</button></a>
                <a href="index.php?pid=<?php echo base64_encode("presentacion/proceso/admision.php") . "&idCaso=" . $_GET["idCaso"] . "&accion=1" ?>"><button class="btn-success btn">Admitir</button></a>
            <?php
            } else if ($caso->getEstado() == "admitido") {
                echo "<strong class='text-success'>El aspirante ha sido admitido</strong>";
            } else if ($caso->getEstado() == "opcionado") {
                echo "<strong class='text-warning'>El aspirante ha sido opcionado</strong><br><br>";
                ?>
                 <strong>Acción:</strong><br>
                <a href="index.php?pid=<?php echo base64_encode("presentacion/proceso/admision.php") . "&idCaso=" . $_GET["idCaso"] . "&accion=3" ?>"><button class="btn-danger btn">Rechazar</button></a>
                <a href="index.php?pid=<?php echo base64_encode("presentacion/proceso/admision.php") . "&idCaso=" . $_GET["idCaso"] . "&accion=1" ?>"><button class="btn-success btn">Admitir</button></a>
                <?php
            } else if ($caso->getEstado() == "rechazado") {
                echo "<strong class='text-danger'>El aspirante ha sido rechazado</strong>";
            } ?>
        </div>
    </div>
</div>
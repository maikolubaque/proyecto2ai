<?php
$nombre = "";
$valor = "";
$cedula = "";
$fecha = "";


if (isset($_POST["registrar"])) {
    $nombre = $_POST["nombre"];
    $valor = $_POST["valor"];
    $fecha = date("yy-m-d g:i ");

    $pin = new Pin("", $valor, "Targeta de credito", $fecha, $nombre);
    $idPin = $pin -> insertar();
    $caso = new Caso("",$fecha, "pago inscripcion", $_SESSION["id"],null,$idPin);
    $caso -> insertar();

    $datosLog = "Valor: ".$valor."; Nombre: ".$nombre;
	$log = new Log("", "Pago inscripcion", $datosLog, date("yy-m-d"), date("g:i a"), $_SESSION["userName"]);
	$log -> insertar();
}
?>

<div class="container mt-2">
    <div class="row justify-content-lg-center">
        <div class="col-lg-8">
            <div class="card">
                <div class="card-header">
                    <h4>Pago Realizado</h4>
                </div>
                <div class="card-body">
                    <div>
                        <strong>Descripción: </strong> El pago se ha realizado con exito,
                        oprima el boton continuar para completar el registro en la plataforma.
                    </div></br>

                    <a href="index.php?pid=<?php echo base64_encode("presentacion/proceso/registrarInfo.php") ?>"><button type="submit" name="registrar" class="btn btn-info">Continuar</button></a> 
                </div>
            </div>
        </div>
    </div>
</div>
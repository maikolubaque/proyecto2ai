<?php
$carrera = new Carrera();
$carreras = $carrera->consultarTodos();

if (isset($_POST["registrar"])) {
    $ciudad = $_POST["ciudad"];
    $direccion = $_POST["direccion"];
    $localidad = $_POST["localidad"];
    $estrato = $_POST["estrato"];
    $colegio = $_POST["colegio"];
    $direccion = $_POST["direccion"];
    $fechaGrado = $_POST["fechaGrado"];
    $icfes = $_POST["icfes"];
    $fecha = date("yy-m-d g:i a");
    $carreraNombre = $_POST["carrera"];
    $carreraId;

    $residencia = new Residencia("", $estrato, $direccion, $localidad, $ciudad, "", $_SESSION["id"], "", "");
    $residencia->registrar();

    $academia = new Academia("", $colegio, $fechaGrado, $icfes, "", $_SESSION["id"], $fecha);
    $academia -> registrar();

    foreach($carreras as $c){
		if($c -> getCarrera() == $carreraNombre){
			$carreraId = $c -> getIdCarrera();
		}
	}

    $caso = new Caso("","","",$_SESSION["id"], $carreraId);
    $caso -> modificarEstadoCarrera("cargue informacion", $carreraId);

    header("Location: index.php?pid=" . base64_encode("presentacion/proceso/preEntrevista.php"));
    
}

?>


<div class="container mt-2">
    <div class="row justify-content-lg-center">
        <div class="col-lg-8">
            <div class="card">
                <div class="card-header">
                    <h4>completar Registro</h4>
                </div>
                <div class="card-body">
                    <?php if (isset($_POST["registrar"])) { ?>

                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            Información Guardada.
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        </div>

                    <?php } ?>
                    <div>
                        <strong>Descripción: </strong> A continuación seleccione la carrera a la que desea hacer la inscripción y
                         complete la información requerida para continuar con el proceso.
                    </div></br>

                    <form action="index.php?pid=<?php echo base64_encode("presentacion/proceso/registrarInfo.php") ?>" method="post" enctype="multipart/form-data">
                        <div class="form-group">
                            <label>Carrera:</label></br>
                            <select id="select-tipo" name="carrera">
                                <?php
                                foreach ($carreras as $cActual) {
                                    echo "<option>";
                                    echo "<a class='dropdown-item' href='#'>" . $cActual->getCarrera() . "</a>";
                                    echo "</option>";
                                }
                                ?>
                            </select>
                            <hr>
                        </div>
                        <h5>Informacion personal</h5>
                        <div class="form-group">
                            <label>Direccion de residencia</label>
                            <input type="text" name="direccion" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label>Ciudad:</label>
                            <input type="text" name="ciudad" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label>Localidad:</label>
                            <input type="text" name="localidad" class="form-control" required>
                        </div>

                        <div class="form-group">
                            <label>Estrato:</label></br>
                            <select id="select-tipo" name="estrato">
                                <option><a class='dropdown-item' href='#'>1</a></option>
                                <option><a class='dropdown-item' href='#'>2</a></option>
                                <option><a class='dropdown-item' href='#'>3</a></option>
                                <option><a class='dropdown-item' href='#'>4</a></option>
                                <option><a class='dropdown-item' href='#'>5</a></option>
                                <option><a class='dropdown-item' href='#'>6</a></option>
                            </select>
                            <hr>
                        </div>
                        <hr>
                        <div class="form-group">
                            <label>Colegio de grado:</label>
                            <input type="text" name="colegio" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label>Fecha de Grado</label>
                            <input type="date" name="fechaGrado" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label>Puntaje ICFES saber 11:</label>
                            <input type="number" name="icfes" class="form-control" min="0" max="500" required>
                        </div>


                        <button type="submit" name="registrar" class="btn btn-info">Registrar</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
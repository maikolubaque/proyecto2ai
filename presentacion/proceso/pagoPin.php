<div class="container mt-2">
    <div class="row justify-content-lg-center">
        <div class="col-lg-8">
            <div class="card">
                <div class="card-header">
                    <h4>Resumen de la compra</h4>
                </div>
                <div class="card-body">
                <strong>Concepto: </strong> Pago inscripcion admisión univesidad
                </br><strong>Valor: </strong> $81.563
                </br><strong>Destinatario: </strong> Universidad Distrital Francisco Jose de Caldas
                </div>
            </div>
        </div>
    </div>
    <div class="row mt-2 justify-content-lg-center">
        <div class="col-lg-8">
            <div class="card">
                <div class="card-header">
                    <h4>Pago</h4>
                </div>
                <div class="card-body">
                 <h5>Targeta de credito</h5>
                 <form action="index.php?pid=<?php echo base64_encode("presentacion/proceso/procesarPago.php") ?>" method="post" enctype="multipart/form-data">
                        <div class="form-group">
                            <label>Numero de la targeta</label>
                            <input type="tel" name="numero" class="form-control"  maxlength="19" placeholder="Card Number" required>
                        </div>
                        <div class="form-group">
                            <label>Nombre del titular</label>
                            <input type="text" name="nombre" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label>Fecha de vencimiento</label>
                            <input type="tel" name="vencimiento" class="form-control"  maxlength="7" placeholder="MM / YY" required>
                        </div>
                        
                        <div class="form-group">
                            <label>Codigo de seguridad</label>
                            <input type="number" name="codSeguridad" class="form-control" pattern="" maxlength="3" placeholder="CVC" required>
                        </div>
                        <input type="hidden" name="valor" class="form-control" value="81563">
                        
                        <button type="submit" name="registrar" class="btn btn-dark">Registrar</button>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
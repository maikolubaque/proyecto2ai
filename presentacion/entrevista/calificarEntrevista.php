<?php

$entrevistaP = new Entrevista_Pregunta();
$respuestas = $entrevistaP -> consultarRespusta($_GET["idCaso"]);

?>


<div class="container mt-2">
    <div class="row mt-2 justify-content-lg-center">
        <div class="col-lg-8">
            <div class="card">
                <div class="card-header">
                    <h4>Solucion de Cuestionario</h4>
                </div>
                <div class="card-body">
                    <?php if (isset($_POST["registrar"])) {
                    ?>
                        <div class="text-center">
                           
                        </div>
                    <?php } ?>
                    <h5>respuestas</h5>
                    <span style="font-size: 14px;">Califique cada una de las respuestas con un puntaje de 0 a 50.</span>
                    <hr>
                    <form action="index.php?pid=<?php echo base64_encode("presentacion/entrevista/procesarCalificacion.php") ?>&idCaso=<?php echo $_GET["idCaso"]?>" method="post" enctype="multipart/form-data">

                        <?php
                        $i = 1;
                        foreach ($respuestas as $p) {
                            echo "<div class='form-group'>";
                            echo "<label>" . $i . ". " . $p->getPregunta() . "</label>";
                            echo "<textarea type='text' class='form-control' required disabled>" . $p->getRespuesta() . "</textarea>";
                            echo "<label>Puntaje: </label>";
                            echo "<input type='number' name=" . $p->getIdEntrevista_Pregunta() . " class='form-control' min='0' max='50' required>";
                            echo "</div>";
                            $i++;
                        }
                        ?>

                        <button type="submit" name="registrar" class="btn btn-dark">Registrar</button>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
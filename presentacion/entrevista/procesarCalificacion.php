<?php

//$caso = new Caso($_GET["idCaso"], "", "", "");

$entrevistaP = new Entrevista_Pregunta();
$respuestas = $entrevistaP->consultarRespusta($_GET["idCaso"]);


$suma = 0;
$idEntrevista;
if (isset($_POST["registrar"])) {
    foreach ($respuestas as $r) {
        $r->editar($_POST[$r->getIdEntrevista_Pregunta()]);
        $suma += $_POST[$r->getIdEntrevista_Pregunta()];
        $idEntrevista = $r -> getEntrevista();
    }

    $promedio = $suma / count($respuestas);
    $promedio = round($promedio);

    $caso = new Caso($_GET["idCaso"]);
    $caso -> consultar2();
    $fecha = date("yy-m-d g:i ");
    $caso -> modificarEstadoFechaCaso("cuestionario calificado", $fecha);

    $entrevista = new Entrevista($idEntrevista, $promedio,"",1,$_SESSION["id"],$_GET["idCaso"] );
    $entrevista -> editar();
}

?>

<div class="container mt-2">
    <div class="row justify-content-lg-center">
        <div class="col-lg-8">
            <div class="card">
                <div class="card-header">
                    <h4>Resumen</h4>
                </div>
                <div class="card-body">
                    <div>
                    <strong>Persona calificada: </strong> <?php echo $caso -> getAspirante(); ?><br>
                    <strong>Calificacion promedio: </strong> <?php echo $promedio; ?><br>
                    <strong>Fecha de respuesta: </strong> <?php echo $caso -> getFecha(); ?><br>
                    <br>
                    <a href="index.php?pid=<?php echo base64_encode("presentacion/Entrevista/consultarEntrevistaPagina.php") ?>"><button type="submit" name="registrar" class="btn btn-info">Volver</button></a>
                </div>
            </div>
        </div>
    </div>
</div>
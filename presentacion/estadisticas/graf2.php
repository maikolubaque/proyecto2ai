<?php
$caso = new Caso();
$carrera = new Carrera();

$carreras = $carrera->consultarTodos();



?>

<script type="text/javascript">
    google.charts.load('current', {
        'packages': ['bar']
    });
    google.charts.setOnLoadCallback(drawChart);

    function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Carrera', 'Admitidos', 'Opcionados', 'Rechazados'],
          <?php
                foreach ($carreras as $c) {
                    $admitidos = $caso->consultarCantidadAdmitidos("admitido", $c->getIdCarrera());
                    $opcionados = $caso->consultarCantidadAdmitidos("opcionado", $c->getIdCarrera());
                    $rechazados = $caso->consultarCantidadAdmitidos("rechazado",  $c->getIdCarrera());
                    echo "['" .$c->getCarrera(). "'," .$admitidos. ",".$opcionados. ",".$rechazados."],";
                }
                
            ?>
        ]);
        var options = {
            chart: {
                title: 'Resultado admisiones por carreras',
            }
        };

        var chart = new google.charts.Bar(document.getElementById('columnchart_material'));

        chart.draw(data, google.charts.Bar.convertOptions(options));
    }
</script>

<div class="container">
    <div class="row">
        <div class="col text-center">
            <div id="columnchart_material" style="width: 700px; height: 500px;"></div>
        </div>
    </div>

</div>
<?php
    $caso = new Caso();
    $admitidos = $caso -> consultarCantidadAdmitidos("admitido");
    $opcionados = $caso -> consultarCantidadAdmitidos("opcionado");
    $rechazados = $caso -> consultarCantidadAdmitidos("rechazado");
?>

<script>
     google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {

        var data = google.visualization.arrayToDataTable(
            [
          ['Estudiantes', 'Admision'],
          ['Admitidos',      <?php echo $admitidos ?>],
          ['Opcionados',       <?php echo $opcionados?>],
          ['Rechazados',   <?php echo $rechazados ?>],
        ]
        );

        var options = {
          title: 'Admitidos, opcionados, Rechazados todas las carreras'
        };

        var chart = new google.visualization.PieChart(document.getElementById('piechart'));
        chart.draw(data, options);

      }

</script>
<div class="container">
    <div class="row">
        <div class="col text-center">
            <div id="piechart" style="width: 650px; height: 500px;"></div>
        </div>
    </div>
   
</div>
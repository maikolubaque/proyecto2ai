<?php
$aspirante = new Aspirante($_SESSION["id"]);
$aspirante->consultar();
?>


<nav class="navbar navbar-expand-md  navbar-dark sticky-top" style="background-color: #0288d1">
    <a class="navbar-brand" href="#">Admisiones</a>
    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navb" aria-expanded="true">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div id="navb" class="navbar-collapse collapse hide">
        <ul class="navbar-nav">
            <li class="nav-item active">
                <a class="nav-link" href="" ) ?>Inicio<span class="sr-only">(current)</span></a>
            </li>

            <li class="nav-item">
                <a class="nav-link disabled" href="#" tabindex="-1" aria-disabled="true">Disabled</a>
            </li>
        </ul>

        <ul class="nav navbar-nav ml-auto">
            <li class="nav-item dropdown"><a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Aspirante: <?php echo $aspirante->getNombre() ?> <?php echo $aspirante->getApellido() ?></a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/aspirante/Cambiar_clave.php") ?>&idAspirante=<?php echo $_SESSION["id"] ?>" method="post">Cambiar Clave</a>
                </div>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="index.php?cerrarSesion=true"><span class="fas fa-sign-in-alt"></span> Cerrar sesion</a>
            </li>
        </ul>
    </div>
</nav>
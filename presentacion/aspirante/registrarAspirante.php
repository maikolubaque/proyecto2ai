<?php
$nombre = "";
$apellido = "";
$cedula = "";
$sexo = "";
$telefono = "";
$correo = "";
$clave = "";
$clave2 = "";
$foto = "";

if (isset($_POST["registrar"])) {
    $nombre = $_POST["nombre"];
    $apellido = $_POST["apellido"];
    $cedula = $_POST["cedula"];
    $sexo = $_POST["sexo"];
    $telefono = $_POST["telefono"];
    $correo = $_POST["correo"];
    $clave = $_POST["clave"];
    $clave2 = $_POST["clave2"];
    $error = "";
    $aspirante = new Aspirante("", $nombre, $apellido, $cedula, $sexo, $telefono, $correo, $clave, "", "");
    if (!$aspirante->existeCorreo()) {
        if ($clave == $clave2) {

            $aspirante->registrar();

            $datosLog = "Nombre: ".$nombre."; Apellido: ".$apellido."; Correo: ".$correo."; sexo: ".$sexo." Documento: ".$cedula;
			$log = new Log("", "Registro aspirante", $datosLog, date("yy-m-d"), date("g:i a"), $nombre." ".$apellido);
			$log -> insertar();
        } else {
            $error = 1;
        }
    } else {
        $error = 2;
    }
}


?>
<div class="container mt-3">
    <div class="row">
        <div class="col-lg-3 col-md-0"></div>
        <div class="col-lg-6 col-md-12">
            <div class="card">
                <div class="card-header text-white bg-dark">
                    <h4>Registro Aspirante</h4>
                </div>
                <div class="card-body">
                    <?php if (isset($_POST["registrar"])) {
                        if ($error == 1) {
                            echo '<div class="alert alert-danger alert-dismissible fade show" role="alert">';
                            echo 'La clave no coincide';
                            echo '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
                            echo '</div>';
                        } else if ($error == 2) {
                            echo '<div class="alert alert-danger alert-dismissible fade show" role="alert">';
                            echo 'el correo ya existe';
                            echo '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
                            echo '</div>';
                        } else {
                    ?>
                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                                Registro exitoso
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            </div>
                    <?php }
                    } ?>
                    <form action="index.php?pid=<?php echo base64_encode("presentacion/aspirante/registrarAspirante.php") ?>" method="post" enctype="multipart/form-data">
                        <div class="form-group">
                            <label>Nombre</label>
                            <input type="text" name="nombre" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label>Apellido</label>
                            <input type="text" name="apellido" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label>Documento</label>
                            <input type="number" name="cedula" class="form-control" required>
                        </div>
                        <div>
                            <label>Sexo Biologico</label></br>
                            <select name="sexo">
                                <option value="hombre">Hombre</option>
                                <option value="mujer" selected>Mujer</option>
                            </select>
                        </div></br>
                        <div class="form-group">
                            <label>Telefono</label>
                            <input type="number" name="telefono" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label>Correo</label>
                            <input type="email" name="correo" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label>Clave</label>
                            <input type="password" name="clave" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label>Confirmar clave</label>
                            <input type="password" name="clave2" class="form-control" required>
                        </div>
                        <button type="submit" name="registrar" class="btn btn-dark">Registrar</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
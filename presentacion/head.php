<?php

?>


<nav class="navbar navbar-expand-md  navbar-dark sticky-top" style="background-color: #0288d1">
  <a class="navbar-brand" href="#">Admisiones Universidad</a>
  <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navb" aria-expanded="true">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div id="navb" class="navbar-collapse collapse hide">
    <ul class="navbar-nav">
    <li class="nav-item active">
        <a class="nav-link" href="index.php?pid=<?php echo base64_encode("presentacion/inicio.php") ?>">Inicio<span class="sr-only">(current)</span></a>
      </li>
      
      
      <li class="nav-item">
        <a class="nav-link disabled" href="#" tabindex="-1" aria-disabled="true"></a>
      </li>
    </ul>

    <ul class="nav navbar-nav ml-auto">
      <li class="nav-item">
        <a class="nav-link" href="index.php?pid=<?php echo base64_encode("presentacion/aspirante/registrarAspirante.php") ?>"><span class="fas fa-user"></span> Registro</a>
      </li>
    </ul>
  </div>
</nav>
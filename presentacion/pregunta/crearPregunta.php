<?php

$pregunta = "";
$carrera = "";
$coordinador = new Coordinador($_SESSION["id"]);
$carrera = $coordinador->consultarCarrera();
$pregunta = new Pregunta("", "", $carrera);

if (isset($_GET["data"])) {
    if ($_GET["data"] == "eliminar") {
        $pre = new Pregunta($_GET["idPregunta"], "", "");
        $pre->eliminar();
    }
}

if (isset($_POST["registrar"])) {
    $preguntaNueva = $_POST["pregunta"];
    $pre = new Pregunta("", $preguntaNueva, $carrera);
    $pre->insertar();
}

$preguntas = $pregunta->consultarTodos();






?>

<div class="container mt-2">
    <div class="row">
        <div class="col-lg-4">
            <div class="card mb-2">
                <div class="card-header text-white bg-dark">
                    <h4>Registrar Nueva Pregunta</h4>
                </div>
                <div class="card-body">
                    <form action="index.php?pid=<?php echo base64_encode("presentacion/pregunta/crearPregunta.php") ?>" method="post" enctype="multipart/form-data">
                        <div class="form-group">
                            <label>Pregunta:</label>
                            <textarea type="text" name="pregunta" class="form-control" required></textarea>
                        </div>
                        <button type="submit" name="registrar" class="btn btn-dark">Registrar</button>
                    </form>
                </div>
                <?php if (isset($_POST["registrar"])) {
                ?>
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        Pregunta registrada con exito.
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    </div>
                <?php } ?>
            </div>
        </div>

        <div class="col-lg-8">
            <div class="card">
                <div class="card-header text-white bg-dark">
                    <h4>Listado de preguntas</h4>
                </div>

                <div class="card-body">
                    <table class=" table table-hover table-striped">
                        <tr>
                            <th>#</th>
                            <th>Pregunta</th>
                            <th></th>
                        </tr>
                        <?php
                        $i = 1;
                        foreach ($preguntas as $preguntaActual) {
                            echo "<tr>";
                            echo "<td>" . $i . "</td>";
                            echo "<td>" . $preguntaActual->getPregunta() . "</td>";
                            echo "<td><a href='index.php?pid=" . base64_encode("presentacion/pregunta/crearPregunta.php") . "&data=eliminar&idPregunta=" . $preguntaActual->getIdPregunta() . "' data-toggle='tooltip' data-placement='left' title='Eliminar'><span class='fas fa-trash'></span></a></td>";
                            echo "</tr>";
                            $i++;
                        }
                        ?>
                    </table>


                </div>
            </div>
        </div>
    </div>
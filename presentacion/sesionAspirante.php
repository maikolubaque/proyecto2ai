<?php
    $caso = new Caso("","","",$_SESSION["id"]);
    $caso -> consultar();

    if($caso -> getEstado() == "pago inscripcion"){
        header("Location: index.php?pid=" . base64_encode("presentacion/proceso/registrarInfo.php"));
    }else if($caso -> getEstado() == "cargue informacion"){
        header("Location: index.php?pid=" . base64_encode("presentacion/proceso/preEntrevista.php"));
    }else if($caso -> getEstado() == "entrevista" || $caso -> getEstado() == "cuestionario calificado" || $caso -> getEstado() == "admitido"|| $caso -> getEstado() == "opcionado"|| $caso -> getEstado() == "rechazado"){
        header("Location: index.php?pid=" . base64_encode("presentacion/proceso/espera.php"));
    }
?>

<div class="container">
    <div class="row">
        <div class="col">
            <h3>Bievenido al sistema de admisiones de la universidad</h3>
            <p>para realizar la aplicación primero deberá pagar el valor correspondiente a la inscripción</p>
            <a href="index.php?pid=<?php echo base64_encode("presentacion/proceso/pagoPin.php") ?>"><button type="button" class="btn btn-info">Ir al pago</button></a>
        </div>

    </div>
</div>
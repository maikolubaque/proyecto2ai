<?php 
$clave_actual = "";
$clave_nueva = "";
$clave_nueva2= "";
$error = "";
if (isset($_POST["cambiar"]))
{
    $clave_actual = $_POST["clave_actual"];
    $clave_nueva =  $_POST["clave_nueva"];
    $clave_nueva2 = $_POST["clave_nueva2"];
    $administrador = new Administrador($_SESSION["id"]);
    $administrador -> consultar();
    if($administrador -> getClave() == md5($clave_actual))
    {
        if($clave_nueva==$clave_nueva2)
        {
            $administrador = new administrador($_SESSION["id"],"","","",$clave_nueva,"");
            $administrador -> editar_clave();
            $error=3;
        }
        else {
            $error=1;
        }
    }else {
            $error=2;
    }
    
    
}

?>


<div class="container-lg">
    <div class="row justify-content-md-center">
        <div class="col-md-6 col-sm-12 mt-4">
            <div class="card">
                <div class="card-header bg-dark text-white">
                    <h4>Cambiar Clave</h4>
                </div>
                <div class="card-body">
                    <form action="index.php?pid=<?php echo base64_encode("presentacion/administrador/editarClave.php") ?>&idAdministrador=<?php echo $_SESSION["id"]?>" method="post" enctype="multipart/form-data">
                        <div class="form-group">
                            <label for="Clave_actual">Clave Actual</label>
                            <input type="password" class="form-control form-control-sm" name="clave_actual" required="">
                        </div>
                        <div class="form-group">
                            <label for="Clave_nueva">Clave Nueva</label>
                            <input type="password" class="form-control form-control-sm" name="clave_nueva" required="">
                        </div>
                        <div class="form-group">
                            <label for="Clave_nueva2">Confirme clave nueva</label>
                            <input type="password" class="form-control form-control-sm" name="clave_nueva2" required="">
                        </div>
                        <button type="submit" name="cambiar" class="btn btn-dark">Cambiar</button>
                    </form>
                    <?php if ($error==1) { ?>
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            las nuevas claves no coinciden
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        </div>
                    <?php } else if($error==2) { ?>
                     	<div class="alert alert-danger alert-dismissible fade show" role="alert">
                            las clave actual no coincide
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        </div>
                    <?php } else if($error==3){ ?>
                    	<div class="alert alert-success alert-dismissible fade show" role="alert">
                            Clave cambiada con exito
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        </div>
                    <?php } ?>
                </div>
            </div>


        </div>
    </div>
</div>

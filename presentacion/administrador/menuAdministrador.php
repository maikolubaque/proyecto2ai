<?php
$administrador = new Administrador($_SESSION["id"]);
$administrador->consultar();
?>
<nav class="navbar navbar-expand-lg navbar-dark sticky-top bg-dark">
  	<a class="navbar-brand"
		href="index.php?pid=<?php echo base64_encode("presentacion/sesionAdministrador.php") ?>"><i
		class="fas fa-home"></i></a>
  <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navb" aria-expanded="true">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div id="navb" class="navbar-collapse collapse hide">
    <ul class="navbar-nav">
     <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Coordinador
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
        <a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/coordinador/registrarCoordinador.php")?>">Nuevo Coordinado</a> 
        <a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/coordinador/consultarCoordinadorPagina.php")?>">Consultar Coordinadores</a> 
        </div>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="index.php?pid=<?php echo base64_encode("presentacion/log/consultarLogPagina.php") ?>">Log</a>
      </li>
    
     
    </ul>

    <ul class="nav navbar-nav ml-auto">
    	<li class="nav-item dropdown"><a
				class="nav-link dropdown-toggle" href="#" id="navbarDropdown"
				role="button" data-toggle="dropdown" aria-haspopup="true"
				aria-expanded="false">Administrador: <?php echo $administrador -> getNombre() ?> <?php echo $administrador -> getApellido() ?></a>
				<div class="dropdown-menu" aria-labelledby="navbarDropdown">
					<a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/administrador/editarAdministrador.php")?>&idAdministrador=<?php echo $_SESSION["id"]?>">Editar Perfil</a> 
					<a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/administrador/editarClave.php")?>&idAdministrador=<?php echo $_SESSION["id"]?>" method="post">Cambiar Clave</a>
				</div>
		</li>
      <li class="nav-item">
        <a class="nav-link" href="index.php?cerrarSesion=true"><span class="fas fa-sign-in-alt"></span> Cerrar sesion</a>
      </li>
    </ul>
  </div>
</nav>
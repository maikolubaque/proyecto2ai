<?php
require_once "logica/Caso.php";
require_once "logica/Carrera.php";
require_once "logica/Aspirante.php";
require_once "logica/Entrevista.php";
require_once "logica/Residencia.php";
require_once "logica/Pin.php";
require_once "logica/Academia.php";
require_once "logica/Profesor.php";
require_once "logica/Entrevista_Pregunta.php";

require_once "ezpdf/class.ezpdf.php";

$caso = new Caso($_GET["idCaso"]);
$caso->consultar3();

$carrera = new Carrera($caso->getCarrera());
$carrera->consultar();

$aspirante = new Aspirante($caso->getAspirante());
$aspirante->consultar();

$pin = new Pin($caso->getPin());
$residencia = new Residencia("", "", "", "", "", "", $aspirante->getIdAspirante());
$entrevista = new Entrevista("", "", "", "", "", $caso->getIdCaso());
$academia = new Academia("", "", "", "", "",  $aspirante->getIdAspirante());
$profesor = new Profesor();

if($caso->getEstado()=="opcionado" || $caso->getEstado()=="admitido" || $caso->getEstado()=="rechazado"){
    $pin -> consultar();
    $residencia->consultar();
    $entrevista->consultar2();
    $academia->consultar();

    $profesor = new Profesor($entrevista -> getProfesor());
    $profesor -> consultar();

    $e_p = new Entrevista_Pregunta("","","","", $entrevista -> getIdEntrevista());
    $respuestas = $e_p -> consultarTodos();
}


$pdf = new Cezpdf("LETTER");
$pdf -> selectFont("ezpdf/fonts/Helvetica.afm");
$pdf -> ezSetCmMargins(2, 2, 3, 3);


$opciones = array("justification" => "center");
$pdf -> ezText("<b>Admisiones</b>", 20, $opciones);
$pdf -> ezText("<b>Reporte proceso de admision</b>", 16, $opciones);

$pdf -> ezText("\n\n<b>Informacion del caso:</b>", 13);
$pdf -> ezText("\n<b>Id del caso: </b>".$caso->getIdCaso(), 13);
$pdf -> ezText("<b>Estado: </b>".$caso->getEstado(), 13);
$pdf -> ezText("<b>Fecha: </b>".$caso->getFecha(), 13);
$pdf -> ezText("<b>Carrera: </b>".$carrera->getCarrera(), 13);

$pdf -> ezText("\n\n<b>Informacion de la persona:</b>", 13);
$pdf -> ezText("\n<b>Nombres: </b>".$aspirante->getNombre(), 13);
$pdf -> ezText("<b>Apellidos: </b>".$aspirante->getApellido(), 13);
$pdf -> ezText("<b>Documento: </b>".$aspirante->getCedula(), 13);
$pdf -> ezText("<b>Sexo: </b>".$aspirante->getSexo(), 13);

$pdf -> ezText("\n<b>Informacion de contacto:</b>", 13);
$pdf -> ezText("<b>Telefono: </b>".$aspirante->getTelefono(), 13);
$pdf -> ezText("<b>Correo: </b>".$aspirante->getCorreo(), 13);

$pdf -> ezText("\n\n<b>Descripcion del proceso seguido por el aspirante</b>", 13, $opciones);

$pdf -> ezText("\n<b>1. Pago de la inscripcion</b>", 13);
$pdf -> ezText("\n   <b>fecha y hora del pago:</b> ".$pin -> getFecha(), 13);
$pdf -> ezText("   <b>Valor cancelado: </b> $ ".$pin -> getPrecio(), 13);
$pdf -> ezText("   <b>Banco: </b> ".$pin -> getBanco(), 13);
$pdf -> ezText("   <b>Nombre de quien cancelo: </b> ".$pin -> getCedula(), 13);

$pdf -> ezText("\n<b>2. Registro de informacion</b>", 13);
$pdf -> ezText("\n   <b>fecha y hora de registro:</b> ".$academia -> getFechaCargue(), 13);
$pdf -> ezText("\n   <b>informacion academica</b>", 13);
$pdf -> ezText("   <b>Colegio de grado:</b> ".$academia -> getColegio(), 13);
$pdf -> ezText("   <b>Fecha de grado:</b> ".$academia -> getFechaGrado(), 13);
$pdf -> ezText("   <b>Puntaje prueba ICFES saber 11:</b> ".$academia -> getIcfes(), 13);

$pdf -> ezNewPage();
$pdf -> ezText("\n   <b>informacion de vivienda</b>", 13);
$pdf -> ezText("   <b>Direccion:</b> ".$residencia -> getDireccion(), 13);
$pdf -> ezText("   <b>Ciudad:</b> ".$residencia -> getCiudad(), 13);
$pdf -> ezText("   <b>Localidad:</b> ".$residencia -> getLocalidad(), 13);
$pdf -> ezText("   <b>Estrato:</b> ".$residencia -> getEstrato(), 13);


$pdf -> ezText("\n<b>3. Cuestionario</b>", 13);
$pdf -> ezText("\n   <b>fecha y hora solucion de cuestionario:</b> ".$entrevista -> getFecha(), 13);
$pdf -> ezText("   <b>Puntaje total obtenido:</b> ".$entrevista -> getPuntaje(), 13);
$pdf -> ezText("   <b>Profesor que evaluo:</b> ".$profesor -> getNombre()." ".$profesor -> getApellido(), 13);

$pdf -> ezText("\n   <b>Solucion de cuestionario</b>", 13);

$encabezados = array("<b>#</b>","<b>Pregunta</b>","<b>respuesta</b>","<b>Calificacion</b>");

$datos = array();
$n = 1;
$i = 0;

if(true){
    while($n-- > 0){
        foreach ($respuestas as $r){
            $datos[$i][0] = $i + 1;
            $datos[$i][1] = $r -> getPregunta();
            $datos[$i][2] = $r -> getRespuesta();
            $datos[$i][3] = $r -> getPuntaje();

            $i++;
        }    
    }
}

$opcionesTabla = array(
    "showLines" => 1,
    "shaded" => 1,
    "shadeCol" => array(0.0, 0.8, 1),
    "rowGap" => 3
);
$pdf -> ezSetDY(-20);
$pdf -> ezTable($datos, $encabezados, "Cuestionario".$nombreCaso."", $opcionesTabla);

$pdf -> ezText("\n<b>4. Resultado de la admision</b>", 13);

if($caso -> getEstado()== "admitido"){
    $pdf -> ezText("\n\n   <b>La persona fue admitida</b> ", 13,);
    $pdf -> ezText("\n   <b>Fecha: </b> ". $caso ->getFecha(), 13,);
}else if($caso -> getEstado()== "opcionado"){
    $pdf -> ezText("\n\n   <b>La persona se encuentra opcionada</b> ", 13,);
    $pdf -> ezText("\n   <b>Fecha: </b> ". $caso ->getFecha(), 13,);
}else if($caso -> getEstado()== "rechazado"){
    $pdf -> ezText("\n\n   <b>El aspirante fue rechazado</b> ", 13,);
    $pdf -> ezText("\n   <b>Fecha: </b> ". $caso ->getFecha(), 13,);
}else{
    $pdf -> ezText("\n\n   <b>Registro incompleto</b> ", 13,);
    $pdf -> ezText("\n   <b>Estado: </b> ". $caso -> getEstado(), 13,);
}

$pdf -> ezStream();
// $pdfcode = $pdf->ezOutput();
// $fp=fopen("reportes/clientes.pdf",'wb');
// fwrite($fp,$pdfcode);
// fclose($fp);

?>
<?php
class ResidenciaDAO{
    private $idResidencia;
    private $estrato;
    private $direccion;
    private $localidad;
    private $ciudad;
    private $certificado;
    private $aspirante;
    private $consteoOtro;
    private $ganancias;  

    public function ResidenciaDAO($idResidencia= "", $estrato = "", $direccion = "", $localidad = "", $ciudad = "", $certificado = "", $aspirante = "", $consteoOtro = "", $ganancias = ""){
        $this -> idResidencia = $idResidencia;
        $this -> estrato = $estrato;
        $this -> direccion = $direccion;
        $this -> localidad = $localidad;
        $this -> ciudad = $ciudad;
        $this -> certificado = $certificado;
        $this -> aspirante = $aspirante;
        $this -> consteoOtro = $consteoOtro;
        $this -> ganancias = $ganancias;
    }

  
    
    public function registrar(){
        return "insert into residencia (estrato, direccion, localidad, ciudad, certificado, idAspirante, costeoOtro, ganancias)
                values ('" . $this -> estrato . "','" . $this -> direccion. "', '".$this -> localidad."' , '".$this -> ciudad."' , '".$this -> certificado."' ,'" . $this -> aspirante . "', '" . $this -> consteoOtro . "', '". $this -> ganancias . "')";
    }
    
  

    public function consultar(){
        return "select estrato, direccion, localidad, ciudad, certificado, idAspirante, costeoOtro, ganancias
                from residencia 
                where idAspirante = '" . $this -> aspirante .  "'";
    }
    
    
    public function consultarPaginacion($cantidad, $pagina){
        return "select o.idAspirante, o.nombre, o.apellido, o.correo,  c.carrera, o.estado
                from aspirante o, carrera c
                where o.idCarrera = c.idCarrera
                limit " . (($pagina-1) * $cantidad) . ", " . $cantidad;
    }
    
    public function consultarCantidad(){
        return "select count(idAspirante)
                from Aspirante";
    }
    
    public function editar(){
        return "update aspirante
                set nombre = '" . $this -> nombre . "', apellido = '" . $this -> apellido . "', correo = '" . $this -> correo . "', estado = '" . $this -> estado . "', idCarrera = '" . $this -> carrera. "'
                where idAspirante = '" . $this -> idAspirante .  "'";
    }
    
}

?>
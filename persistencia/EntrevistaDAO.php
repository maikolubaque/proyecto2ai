<?php
class EntrevistaDAO{
    private $idEntrevista;
    private $puntajeT;
    private $fecha;
    private $estado;
    private $profesor;
    private $caso;

    public function EntrevistaDAO($idEntrevista = "", $puntajeT = "", $fecha= "", $estado="", $profesor="", $caso=""){
        $this -> idEntrevista = $idEntrevista;
        $this -> puntajeT = $puntajeT;
        $this -> fecha = $fecha;
        $this -> estado = $estado;
        $this -> profesor = $profesor;
        $this -> caso = $caso;            
    }
    
    public function registrar(){
        return "insert into entrevista (puntajeT, fecha, estado, idProfesor, idCaso)
                values (0,'" . $this -> fecha. "', '" . $this -> estado . "',null, " .$this -> caso . ")";
    }

    public function consultar(){
        return "select idEntrevista, puntajeT, fecha, estado, idProfesor, idCaso
                from entrevista 
                where idEntrevista = '" . $this -> idEntrevista .  "'";
    }

    public function consultar2(){
        return "select idEntrevista, puntajeT, fecha, estado, idProfesor
                from entrevista 
                where idCaso = '" . $this -> caso .  "'";
    }
    
    
    public function consultarPaginacion($cantidad, $pagina){
        return "select idEntrevista, puntajeT, fecha, estado, idProfesor, idCaso
                from entrevista 
                limit " . (($pagina-1) * $cantidad) . ", " . $cantidad;
    }
    
    public function consultarCantidad(){
        return "select count(idEntrevista)
                from entrevista";
    }
    
    public function editar(){
        return "update entrevista
                set puntajeT = '" . $this -> puntajeT . "', estado = '" . $this -> estado . "', idProfesor = '" . $this -> profesor . "'
                where idEntrevista = '" . $this -> idEntrevista .  "'";
    }
    

    public function cambiarEstado($estado){
        return "update entrevista
                set estado = '" . $estado . "'
                where idEntrevista = '" . $this -> idEntrevista .  "'";
    }

    public function consultarId(){
        return "select @@identity AS idEntrevista";
    }
    
}

?>
<?php
class Entrevista_PreguntaDAO{
    private $idEntrevista_Pregunta;
    private $pregunta;
    private $respuesta;
    private $puntaje;
    private $entrevista;


       
    public function Entrevista_PreguntaDAO($idEntrevista_Pregunta="", $pregunta = "", $respuesta = "",  $puntaje = "",  $entrevista = ""){
        $this -> idEntrevista_Pregunta = $idEntrevista_Pregunta;
        $this -> pregunta = $pregunta;
        $this -> respuesta = $respuesta;
        $this -> puntaje = $puntaje;
        $this -> entrevista = $entrevista;
    }

    public function consultar(){
        return "select pregunta, respuesta, puntaje, idEntrevista
                from entrevista_pregunta
                where idEntrevista = '" . $this -> entrevista .  "'";
    }    
    
    public function insertar(){
        return "insert into entrevista_pregunta (pregunta, respuesta, puntaje, idEntrevista)
                values ('" . $this -> pregunta . "', '" . $this -> respuesta . "', 0, '".$this -> entrevista ."')";
    }
    
    public function consultarTodos(){
        return "select idEntrevista_pregunta,pregunta, respuesta, puntaje
                from entrevista_pregunta
                where idEntrevista=".$this -> entrevista."";
    }

    public function consultarRespuesta($caso){
        return "select e_p.*
                from entrevista_pregunta e_p, entrevista e, caso c
                WHERE e_p.idEntrevista = e.idEntrevista and e.idCaso = c.idCaso and c.idCaso = ".$caso."";
    }
    
    public function editar($nota){
        return "update entrevista_pregunta
                set puntaje = ". $nota."
                where idEntrevista_Pregunta = " . $this -> idEntrevista_Pregunta .  "";
    }
    
}

?>

<?php
class AspiranteDAO{
    private $idAspirante;
    private $nombre;
    private $apellido;
    private $cedula;
    private $sexo;
    private $telefono;
    private $correo;
    private $clave;
    private $carrera;
    private $estado;

    public function AspiranteDAO($idAspirante= "", $nombre = "", $apellido = "", $cedula = "", $sexo = "", $telefono = "", $correo = "", $clave = "", $carrera = "", $estado = ""){
        $this -> idAspirante = $idAspirante;
        $this -> nombre = $nombre;
        $this -> apellido = $apellido;
        $this -> cedula = $cedula;
        $this -> sexo = $sexo;
        $this -> telefono = $telefono;
        $this -> correo = $correo;
        $this -> clave = $clave;
        $this -> carrera = $carrera;
        $this -> estado = $estado;        
    }

    public function existeCorreo(){
        return "select correo
                from aspirante
                where correo = '" . $this -> correo .  "'";
    }
    
    public function registrar(){
        return "insert into aspirante (nombre, apellido, cedula, sexo, telefono, correo, clave, estado)
                values ('" . $this -> nombre . "','" . $this -> apellido. "', '".$this -> cedula."' , '".$this -> sexo."' , '".$this -> telefono."' ,'" . $this -> correo . "', '" . md5($this -> clave) . "', '1')";
    }
    
    
    public function autenticar(){
        return "select idAspirante, estado, nombre, apellido
                from aspirante
                where correo = '" . $this -> correo .  "' and clave = '" . md5($this -> clave) . "'";
    }

    public function consultar(){
        return "select nombre, apellido, cedula, sexo, telefono, correo, clave, estado
                from aspirante 
                where idAspirante = '" . $this -> idAspirante .  "'";
    }
    
    
    public function consultarPaginacion($cantidad, $pagina){
        return "select o.idAspirante, o.nombre, o.apellido, o.correo,  c.carrera, o.estado
                from aspirante o, carrera c
                where o.idCarrera = c.idCarrera
                limit " . (($pagina-1) * $cantidad) . ", " . $cantidad;
    }
    
    public function consultarCantidad(){
        return "select count(idAspirante)
                from Aspirante";
    }
    
    public function editar(){
        return "update aspirante
                set nombre = '" . $this -> nombre . "', apellido = '" . $this -> apellido . "', correo = '" . $this -> correo . "', estado = '" . $this -> estado . "', idCarrera = '" . $this -> carrera. "'
                where idAspirante = '" . $this -> idAspirante .  "'";
    }
    
    public function editar_clave(){
        return "update Aspirante
                set clave = '" . md5($this -> clave) . "'
                where idAspirante = '" . $this -> idAspirante .  "'";
    }

    public function cambiarEstado($estado){
        return "update Aspirante
                set estado = '" . $estado . "'
                where idAspirante = '" . $this -> idAspirante .  "'";
    }

    public function consultarCarrera(){
        return "select idCarrera
                from aspirante
                where idAspirante = '" . $this -> idAspirante .  "'";
    }
    
}

?>
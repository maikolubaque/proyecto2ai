-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 26-08-2020 a las 20:31:43
-- Versión del servidor: 10.4.14-MariaDB
-- Versión de PHP: 7.4.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

create database bdadmisionai;
use bdadmisionai;

--
-- Base de datos: `bdadmisionai`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `academia`
--

CREATE TABLE `academia` (
  `idAcademia` int(11) NOT NULL,
  `colegio` varchar(100) NOT NULL,
  `fechaGrado` date NOT NULL,
  `icfes` int(11) NOT NULL,
  `certificado` varchar(80) DEFAULT NULL,
  `idAspirante` int(11) NOT NULL,
  `fechaCargue` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `academia`
--

INSERT INTO `academia` (`idAcademia`, `colegio`, `fechaGrado`, `icfes`, `certificado`, `idAspirante`, `fechaCargue`) VALUES
(4, 'acacia ll', '2020-08-02', 320, '', 14, '2020-08-24 12:38:00'),
(5, 'IED union europea', '2018-06-21', 350, '', 15, '2020-08-25 05:01:00'),
(6, 'IED san marcos', '2020-08-03', 356, '', 16, '2020-08-25 06:26:00'),
(7, 'acacia ll', '2020-08-04', 200, '', 17, '2020-08-25 06:48:00'),
(8, 'inem', '2020-08-05', 402, '', 18, '2020-08-26 01:08:00'),
(9, 'IED san marcos', '2020-08-11', 253, '', 19, '2020-08-26 01:21:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `administrador`
--

CREATE TABLE `administrador` (
  `idAdministrador` int(11) NOT NULL,
  `nombre` varchar(45) NOT NULL,
  `apellido` varchar(45) NOT NULL,
  `correo` varchar(45) NOT NULL,
  `clave` varchar(45) NOT NULL,
  `foto` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `administrador`
--

INSERT INTO `administrador` (`idAdministrador`, `nombre`, `apellido`, `correo`, `clave`, `foto`) VALUES
(1, 'andres eduardo', 'priento', '123@123.com', '202cb962ac59075b964b07152d234b70', 'img/1598396714.jpg');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `aspirante`
--

CREATE TABLE `aspirante` (
  `idAspirante` int(11) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `apellido` varchar(100) NOT NULL,
  `cedula` varchar(20) DEFAULT NULL,
  `sexo` varchar(45) DEFAULT NULL,
  `telefono` varchar(45) DEFAULT NULL,
  `correo` varchar(45) NOT NULL,
  `clave` varchar(45) NOT NULL,
  `estado` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `aspirante`
--

INSERT INTO `aspirante` (`idAspirante`, `nombre`, `apellido`, `cedula`, `sexo`, `telefono`, `correo`, `clave`, `estado`) VALUES
(14, 'carlos andres', 'martinez caballero', '102256988', 'hombre', '320356985', '444@444.com', '550a141f12de6341fba65b0ad0433500', 1),
(15, 'pedro david', 'peña nieto', '102253698', 'hombre', '30256358654', '446@446.com', '1a5b1e4daae265b790965a275b53ae50', 1),
(16, 'sol andrea ', 'peña morales', '1025569552', 'mujer', '2566566592', '445@445.com', '67f7fb873eaf29526a11a9b7ac33bfac', 1),
(17, 'james adolfo', 'rodriguez muña', '125663655', 'hombre', '32032569885', '447@447.com', '9a96876e2f8f3dc4f3cf45f02c61c0c1', 1),
(18, 'sandra alejandra ', 'guzman', '10255636658', 'mujer', '32036988568', '448@448.com', '9b70e8fe62e40c570a322f1b0b659098', 1),
(19, 'sebastian alejandro', 'rodriguez', '2563369855', 'hombre', '26656562626', '449@449.com', 'd61e4bbd6393c9111e6526ea173a7c8b', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `carrera`
--

CREATE TABLE `carrera` (
  `idCarrera` int(11) NOT NULL,
  `carrera` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `carrera`
--

INSERT INTO `carrera` (`idCarrera`, `carrera`) VALUES
(1, 'tecnología industrial'),
(2, 'tecnología en sistematizacion de datos');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `caso`
--

CREATE TABLE `caso` (
  `idCaso` int(11) NOT NULL,
  `fecha` date NOT NULL,
  `estado` varchar(50) NOT NULL,
  `idAspirante` int(11) DEFAULT NULL,
  `idCarrera` int(11) DEFAULT NULL,
  `idPin` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `caso`
--

INSERT INTO `caso` (`idCaso`, `fecha`, `estado`, `idAspirante`, `idCarrera`, `idPin`) VALUES
(18, '2020-08-24', 'admitido', 14, 1, 25),
(19, '2020-08-25', 'admitido', 15, 2, 26),
(20, '2020-08-25', 'rechazado', 16, 2, 27),
(21, '2020-08-25', 'cargue informacion', 17, 2, 28),
(22, '2020-08-26', 'rechazado', 18, 1, 29),
(23, '2020-08-26', 'rechazado', 19, 2, 30);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `coordinador`
--

CREATE TABLE `coordinador` (
  `idCoordinador` int(11) NOT NULL,
  `nombre` varchar(45) NOT NULL,
  `apellido` varchar(45) NOT NULL,
  `correo` varchar(45) NOT NULL,
  `clave` varchar(45) NOT NULL,
  `estado` int(11) NOT NULL,
  `idCarrera` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `coordinador`
--

INSERT INTO `coordinador` (`idCoordinador`, `nombre`, `apellido`, `correo`, `clave`, `estado`, `idCarrera`) VALUES
(1, 'Maria', 'Perez', '222@222.com', 'bcbe3365e6ac95ea2c0343a2395834dd', 1, 2),
(3, 'carlos', 'garcia', '223@223.com', '115f89503138416a242f40fb7d7f338e', 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `entrevista`
--

CREATE TABLE `entrevista` (
  `idEntrevista` int(11) NOT NULL,
  `puntajeT` int(11) DEFAULT NULL,
  `fecha` datetime NOT NULL,
  `estado` int(11) NOT NULL,
  `idProfesor` int(11) DEFAULT NULL,
  `idCaso` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `entrevista`
--

INSERT INTO `entrevista` (`idEntrevista`, `puntajeT`, `fecha`, `estado`, `idProfesor`, `idCaso`) VALUES
(20, 36, '2020-08-24 00:00:00', 1, 1, 18),
(21, 40, '2020-08-25 17:07:00', 1, 2, 19),
(22, 45, '2020-08-25 18:28:00', 1, 2, 20),
(23, 28, '2020-08-26 13:10:00', 1, 1, 22),
(24, 17, '2020-08-26 13:22:00', 1, 2, 23);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `entrevista_pregunta`
--

CREATE TABLE `entrevista_pregunta` (
  `idEntrevista_Pregunta` int(11) NOT NULL,
  `pregunta` varchar(300) NOT NULL,
  `respuesta` varchar(400) NOT NULL,
  `puntaje` int(11) DEFAULT NULL,
  `idEntrevista` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `entrevista_pregunta`
--

INSERT INTO `entrevista_pregunta` (`idEntrevista_Pregunta`, `pregunta`, `respuesta`, `puntaje`, `idEntrevista`) VALUES
(86, 'porque eligio esta carrera', 'me gusta', 40, 20),
(87, 'en donde vive', 'bogota', 30, 20),
(88, 'con quien vive', 'padres', 35, 20),
(89, 'porque quiere estudiar', 'progresar', 40, 20),
(90, '¿que sabe de sistemas?', 'ejemplo pregunta 1', 45, 21),
(91, '¿con quien vive?', 'solo', 35, 21),
(92, '¿por que quiere estudiar esta carrera?', 'ejemplo 3', 40, 21),
(93, '¿que sabe de sistemas?', 'mucho', 50, 22),
(94, '¿con quien vive?', 'solo', 45, 22),
(95, '¿por que quiere estudiar esta carrera?', 'me apasiona', 40, 22),
(96, 'porque eligio esta carrera', 'me gusta', 20, 23),
(97, 'en donde vive', 'bogota', 35, 23),
(98, 'con quien vive', 'solo', 25, 23),
(99, 'porque quiere estudiar', 'progresar', 30, 23),
(100, '¿que sabe de sistemas?', 'nada', 0, 24),
(101, '¿con quien vive?', 'papas', 20, 24),
(102, '¿por que quiere estudiar esta carrera?', 'porque si', 30, 24);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `log`
--

CREATE TABLE `log` (
  `idLog` int(11) NOT NULL,
  `accion` varchar(300) NOT NULL,
  `datos` varchar(300) NOT NULL,
  `fecha` date NOT NULL,
  `hora` time NOT NULL,
  `actor` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `log`
--

INSERT INTO `log` (`idLog`, `accion`, `datos`, `fecha`, `hora`, `actor`) VALUES
(0, 'Ingreso al sistema', 'Rol: Administrador', '2020-08-26', '12:50:00', 'andres eduardo priento'),
(0, 'Ingreso al sistema', 'Rol: Coordinador', '2020-08-26', '12:50:00', ' '),
(0, 'Ingreso al sistema', 'Rol: Coordinador', '2020-08-26', '12:50:00', 'Maria Perez'),
(0, 'Ingreso al sistema', 'Rol: Administrador', '2020-08-26', '12:50:00', 'andres eduardo priento'),
(0, 'Registro aspirante', 'Nombre: sandra alejandra ; Apellido: guzman; Correo: 448@448.com; sexo: mujer Documento: 10255636658', '2020-08-26', '12:54:00', 'sandra alejandra  guzman'),
(0, 'Ingreso al sistema', 'Rol: Administrador', '2020-08-26', '12:54:00', 'andres eduardo priento'),
(0, 'Ingreso al sistema', 'Rol: Coordinador', '2020-08-26', '12:58:00', ' '),
(0, 'Pago inscripcion', 'Valor: 81563; Nombre: cristian', '2020-08-26', '12:58:00', 'sandra alejandra  guzman'),
(0, 'Ingreso al sistema', 'Rol: Coordinador', '2020-08-26', '01:00:00', 'Maria Perez'),
(0, 'Modificar profesor', 'Nombre: pedro camilo; Apellido: duque; Correo: 333@333.com', '2020-08-26', '01:03:00', 'Maria Perez'),
(0, 'Ingreso al sistema', 'Rol: Administrador', '2020-08-26', '01:05:00', 'andres eduardo priento'),
(0, 'Ingreso al sistema', 'Rol: Coordinador', '2020-08-26', '01:06:00', 'Maria Perez'),
(0, 'Ingreso al sistema', 'Rol: Coordinador', '2020-08-26', '01:06:00', 'carlos garcia'),
(0, 'Ingreso al sistema', 'Rol: Aspirante', '2020-08-26', '01:07:00', ' '),
(0, 'Ingreso al sistema', 'Rol: Coordinador', '2020-08-26', '01:09:00', 'carlos garcia'),
(0, 'Ingreso al sistema', 'Rol: Aspirante', '2020-08-26', '01:09:00', ' '),
(0, 'Ingreso al sistema', 'Rol: Coordinador', '2020-08-26', '01:10:00', 'carlos garcia'),
(0, 'Ingreso al sistema', 'Rol: profesor', '2020-08-26', '01:10:00', 'pedro camilo duque'),
(0, 'Ingreso al sistema', 'Rol: Coordinador', '2020-08-26', '01:12:00', 'carlos garcia'),
(0, 'Registro aspirante', 'Nombre: sebastian alejandro; Apellido: rodriguez; Correo: 449@449.com; sexo: hombre Documento: 2563369855', '2020-08-26', '01:18:00', 'sebastian alejandro rodriguez'),
(0, 'Ingreso al sistema', 'Rol: Aspirante', '2020-08-26', '01:18:00', ' '),
(0, 'Pago inscripcion', 'Valor: 81563; Nombre: juan david', '2020-08-26', '01:19:00', 'sebastian alejandro rodriguez'),
(0, 'Ingreso al sistema', 'Rol: profesor', '2020-08-26', '01:22:00', 'pedro camilo duque'),
(0, 'Ingreso al sistema', 'Rol: profesor', '2020-08-26', '01:22:00', 'carmen benites'),
(0, 'Ingreso al sistema', 'Rol: Coordinador', '2020-08-26', '01:23:00', 'carlos garcia'),
(0, 'Ingreso al sistema', 'Rol: Coordinador', '2020-08-26', '01:23:00', 'Maria Perez'),
(0, 'Modificar profesor', 'Nombre: falcao eduardo; Apellido: garcia; Correo: 335@335.com', '2020-08-26', '01:27:00', 'Maria Perez'),
(0, 'Ingreso al sistema', 'Rol: Administrador', '2020-08-26', '01:28:00', 'andres eduardo priento'),
(0, 'Ingreso al sistema', 'Rol: Administrador', '2020-08-26', '01:28:00', 'andres eduardo priento'),
(0, 'Ingreso al sistema', 'Rol: Aspirante', '2020-08-26', '01:30:00', ' ');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pin`
--

CREATE TABLE `pin` (
  `idPin` int(11) NOT NULL,
  `precioT` double NOT NULL,
  `banco` varchar(100) NOT NULL,
  `fecha` datetime NOT NULL,
  `cedula` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `pin`
--

INSERT INTO `pin` (`idPin`, `precioT`, `banco`, `fecha`, `cedula`) VALUES
(25, 81563, 'Targeta de credito', '2020-08-24 12:37:00', 'andrea  martinez'),
(26, 81563, 'Targeta de credito', '2020-08-25 04:56:00', 'maria sol peña'),
(27, 81563, 'Targeta de credito', '2020-08-25 06:22:00', 'juan david'),
(28, 81563, 'Targeta de credito', '2020-08-25 06:48:00', 'garzon'),
(29, 81563, 'Targeta de credito', '2020-08-26 12:58:00', 'cristian'),
(30, 81563, 'Targeta de credito', '2020-08-26 01:19:00', 'juan david');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pregunta`
--

CREATE TABLE `pregunta` (
  `idPregunta` int(11) NOT NULL,
  `pregunta` varchar(300) NOT NULL,
  `idCarrera` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `pregunta`
--

INSERT INTO `pregunta` (`idPregunta`, `pregunta`, `idCarrera`) VALUES
(1, 'porque eligio esta carrera', 1),
(7, 'en donde vive', 1),
(8, 'con quien vive', 1),
(11, 'porque quiere estudiar', 1),
(12, '¿que sabe de sistemas?', 2),
(13, '¿con quien vive?', 2),
(14, '¿por que quiere estudiar esta carrera?', 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `profesor`
--

CREATE TABLE `profesor` (
  `idProfesor` int(11) NOT NULL,
  `nombre` varchar(45) NOT NULL,
  `apellido` varchar(45) NOT NULL,
  `correo` varchar(45) NOT NULL,
  `clave` varchar(45) NOT NULL,
  `estado` int(11) NOT NULL,
  `idCarrera` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `profesor`
--

INSERT INTO `profesor` (`idProfesor`, `nombre`, `apellido`, `correo`, `clave`, `estado`, `idCarrera`) VALUES
(1, 'pedro camilo', 'duque', '333@333.com', '310dcbbf4cce62f762a2aaa148d556bd', 1, 1),
(2, 'carmen', 'benites', '334@334.com', '2f2b265625d76a6704b08093c652fd79', 1, 2),
(3, 'falcao eduardo', 'garcia', '335@335.com', 'f9b902fc3289af4dd08de5d1de54f68f', 0, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `residencia`
--

CREATE TABLE `residencia` (
  `idResidencia` int(11) NOT NULL,
  `estrato` int(11) NOT NULL,
  `direccion` varchar(100) NOT NULL,
  `localidad` varchar(80) NOT NULL,
  `ciudad` varchar(100) NOT NULL,
  `certificado` varchar(100) NOT NULL,
  `idAspirante` int(11) NOT NULL,
  `costeoOtro` tinyint(4) NOT NULL,
  `ganancias` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `residencia`
--

INSERT INTO `residencia` (`idResidencia`, `estrato`, `direccion`, `localidad`, `ciudad`, `certificado`, `idAspirante`, `costeoOtro`, `ganancias`) VALUES
(21, 2, 'calle 62a 19b', 'usme', 'bogota', '', 14, 0, ''),
(22, 3, 'cll 52 #26 -56', 'suba', 'bogota', '', 15, 0, ''),
(23, 2, 'cll 52 #26 -56', 'ciudad bolivar', 'bogota', '', 16, 0, ''),
(24, 4, 'calle 62a', 'ciudad bolivar', 'bogota', '', 17, 0, ''),
(25, 2, 'cll 52 #26 -56', 'usme', 'bogota', '', 18, 0, ''),
(26, 3, 'calle 62a', 'usme', 'bogota', '', 19, 0, '');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `academia`
--
ALTER TABLE `academia`
  ADD PRIMARY KEY (`idAcademia`,`idAspirante`),
  ADD KEY `fk_Academia_Aspirante1` (`idAspirante`);

--
-- Indices de la tabla `administrador`
--
ALTER TABLE `administrador`
  ADD PRIMARY KEY (`idAdministrador`);

--
-- Indices de la tabla `aspirante`
--
ALTER TABLE `aspirante`
  ADD PRIMARY KEY (`idAspirante`);

--
-- Indices de la tabla `carrera`
--
ALTER TABLE `carrera`
  ADD PRIMARY KEY (`idCarrera`);

--
-- Indices de la tabla `caso`
--
ALTER TABLE `caso`
  ADD PRIMARY KEY (`idCaso`) USING BTREE,
  ADD KEY `fk_Caso_Aspirante1` (`idAspirante`),
  ADD KEY `fk_Caso_Carrera1` (`idCarrera`),
  ADD KEY `fk_Caso_Pin1` (`idPin`);

--
-- Indices de la tabla `coordinador`
--
ALTER TABLE `coordinador`
  ADD PRIMARY KEY (`idCoordinador`,`idCarrera`),
  ADD KEY `fk_Coodinador_Carrera` (`idCarrera`);

--
-- Indices de la tabla `entrevista`
--
ALTER TABLE `entrevista`
  ADD PRIMARY KEY (`idEntrevista`,`idCaso`) USING BTREE,
  ADD KEY `fk_Entrevista_Profesor1` (`idProfesor`),
  ADD KEY `fk_Entrevista_Caso1` (`idCaso`);

--
-- Indices de la tabla `entrevista_pregunta`
--
ALTER TABLE `entrevista_pregunta`
  ADD PRIMARY KEY (`idEntrevista_Pregunta`,`idEntrevista`),
  ADD KEY `fk_Entrevista_Pregunta_Entrevista1` (`idEntrevista`);

--
-- Indices de la tabla `pin`
--
ALTER TABLE `pin`
  ADD PRIMARY KEY (`idPin`);

--
-- Indices de la tabla `pregunta`
--
ALTER TABLE `pregunta`
  ADD PRIMARY KEY (`idPregunta`,`idCarrera`),
  ADD KEY `fk_Pregunta_Carrera1` (`idCarrera`);

--
-- Indices de la tabla `profesor`
--
ALTER TABLE `profesor`
  ADD PRIMARY KEY (`idProfesor`,`idCarrera`),
  ADD KEY `fk_Coodinador_Carrera0` (`idCarrera`);

--
-- Indices de la tabla `residencia`
--
ALTER TABLE `residencia`
  ADD PRIMARY KEY (`idResidencia`,`idAspirante`),
  ADD KEY `fk_Academia_Aspirante10` (`idAspirante`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `academia`
--
ALTER TABLE `academia`
  MODIFY `idAcademia` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de la tabla `administrador`
--
ALTER TABLE `administrador`
  MODIFY `idAdministrador` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `aspirante`
--
ALTER TABLE `aspirante`
  MODIFY `idAspirante` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT de la tabla `carrera`
--
ALTER TABLE `carrera`
  MODIFY `idCarrera` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `caso`
--
ALTER TABLE `caso`
  MODIFY `idCaso` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT de la tabla `coordinador`
--
ALTER TABLE `coordinador`
  MODIFY `idCoordinador` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `entrevista`
--
ALTER TABLE `entrevista`
  MODIFY `idEntrevista` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT de la tabla `entrevista_pregunta`
--
ALTER TABLE `entrevista_pregunta`
  MODIFY `idEntrevista_Pregunta` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=103;

--
-- AUTO_INCREMENT de la tabla `pin`
--
ALTER TABLE `pin`
  MODIFY `idPin` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT de la tabla `pregunta`
--
ALTER TABLE `pregunta`
  MODIFY `idPregunta` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT de la tabla `profesor`
--
ALTER TABLE `profesor`
  MODIFY `idProfesor` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `residencia`
--
ALTER TABLE `residencia`
  MODIFY `idResidencia` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `academia`
--
ALTER TABLE `academia`
  ADD CONSTRAINT `fk_Academia_Aspirante1` FOREIGN KEY (`idAspirante`) REFERENCES `aspirante` (`idAspirante`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `caso`
--
ALTER TABLE `caso`
  ADD CONSTRAINT `fk_Caso_Aspirante1` FOREIGN KEY (`idAspirante`) REFERENCES `aspirante` (`idAspirante`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Caso_Carrera1` FOREIGN KEY (`idCarrera`) REFERENCES `carrera` (`idCarrera`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Caso_Pin1` FOREIGN KEY (`idPin`) REFERENCES `pin` (`idPin`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `coordinador`
--
ALTER TABLE `coordinador`
  ADD CONSTRAINT `fk_Coodinador_Carrera` FOREIGN KEY (`idCarrera`) REFERENCES `carrera` (`idCarrera`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `entrevista`
--
ALTER TABLE `entrevista`
  ADD CONSTRAINT `fk_Entrevista_Caso1` FOREIGN KEY (`idCaso`) REFERENCES `caso` (`idCaso`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Entrevista_Profesor1` FOREIGN KEY (`idProfesor`) REFERENCES `profesor` (`idProfesor`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `profesor`
--
ALTER TABLE `profesor`
  ADD CONSTRAINT `fk_Coodinador_Carrera0` FOREIGN KEY (`idCarrera`) REFERENCES `carrera` (`idCarrera`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

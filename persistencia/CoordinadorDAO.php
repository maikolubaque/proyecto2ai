<?php
class CoordinadorDAO{
    private $idCoordinador;
    private $nombre;
    private $apellido;
    private $correo;
    private $clave;
    private $carrera;
    private $estado;

    public function CoordinadorDAO($idCoordinador= "", $nombre = "", $apellido = "", $correo = "", $clave = "", $carrera = "", $estado = ""){
        $this -> idCoordinador = $idCoordinador;
        $this -> nombre = $nombre;
        $this -> apellido = $apellido;
        $this -> correo = $correo;
        $this -> clave = $clave;
        $this -> carrera = $carrera;
        $this -> estado = $estado;        
    }

    public function existeCorreo(){
        return "select correo
                from coordinador
                where correo = '" . $this -> correo .  "'";
    }
    
    public function registrar(){
        return "insert into coordinador (nombre, apellido, correo, clave, idCarrera, estado)
                values ('" . $this -> nombre . "','" . $this -> apellido. "', '" . $this -> correo . "', '" . md5($this -> clave) . "','" . $this -> carrera . "', '1')";
    }
    
    
    public function autenticar(){
        return "select idCoordinador, estado, nombre, apellido
                from coordinador 
                where correo = '" . $this -> correo .  "' and clave = '" . md5($this -> clave) . "'";
    }

    public function consultar(){
        return "select o.nombre, o.apellido, o.correo, o.clave, o.idCarrera, estado, c.carrera, c.idCarrera
                from coordinador o, carrera c
                where o.idCoordinador = '" . $this -> idCoordinador .  "' and o.idCarrera = c.idCarrera";
    }
    
    
    public function consultarPaginacion($cantidad, $pagina){
        return "select o.idCoordinador, o.nombre, o.apellido, o.correo, o.estado, c.carrera , o.idCarrera
                from coordinador o, carrera c
                where o.idCarrera = c.idCarrera
                limit " . (($pagina-1) * $cantidad) . ", " . $cantidad;
    }
    
    public function consultarCantidad(){
        return "select count(idCoordinador)
                from coordinador";
    }
    
    public function editar(){
        return "update coordinador
                set nombre = '" . $this -> nombre . "', apellido = '" . $this -> apellido . "', correo = '" . $this -> correo . "', idCarrera = '" . $this -> carrera. "'
                where idCoordinador = '" . $this -> idCoordinador .  "'";
    }
    
    public function editar_clave(){
        return "update coordinador
                set clave = '" . md5($this -> clave) . "'
                where idCoordinador = '" . $this -> idCoordinador .  "'";
    }

    public function cambiarEstado($estado){
        return "update coordinador
                set estado = '" . $estado . "'
                where idCoordinador = '" . $this -> idCoordinador .  "'";
    }

    public function consultarCarrera(){
        return "select idCarrera
                from coordinador
                where idCoordinador = '" . $this -> idCoordinador .  "'";
    }
    
}

?>
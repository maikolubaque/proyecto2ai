<?php
class ProfesorDAO{
    private $idProfesor;
    private $nombre;
    private $apellido;
    private $correo;
    private $clave;
    private $carrera;
    private $estado;

    public function ProfesorDAO($idProfesor= "", $nombre = "", $apellido = "", $correo = "", $clave = "", $carrera = "", $estado = ""){
        $this -> idProfesor = $idProfesor;
        $this -> nombre = $nombre;
        $this -> apellido = $apellido;
        $this -> correo = $correo;
        $this -> clave = $clave;
        $this -> carrera = $carrera;
        $this -> estado = $estado;        
    }

    public function existeCorreo(){
        return "select correo
                from profesor
                where correo = '" . $this -> correo .  "'";
    }
    
    public function registrar(){
        return "insert into profesor (nombre, apellido, correo, clave, idCarrera, estado)
                values ('" . $this -> nombre . "','" . $this -> apellido. "', '" . $this -> correo . "', '" . md5($this -> clave) . "','" . $this -> carrera . "', '1')";
    }
    
    
    public function autenticar(){
        return "select idProfesor, estado, nombre, apellido
                from profesor
                where correo = '" . $this -> correo .  "' and clave = '" . md5($this -> clave) . "'";
    }

    public function consultar(){
        return "select o.nombre, o.apellido, o.correo, o.clave, c.carrera, o.estado
                from profesor o, carrera c
                where idProfesor = '" . $this -> idProfesor .  "' and c.idCarrera = o.idCarrera";
    }
    
    
    public function consultarPaginacion($cantidad, $pagina){
        return "select o.idProfesor, o.nombre, o.apellido, o.correo,  c.carrera, o.estado
                from profesor o, carrera c
                where o.idCarrera = c.idCarrera
                limit " . (($pagina-1) * $cantidad) . ", " . $cantidad;
    }
    
    public function consultarCantidad(){
        return "select count(idProfesor)
                from profesor";
    }
    
    public function editar(){
        return "update profesor
                set nombre = '" . $this -> nombre . "', apellido = '" . $this -> apellido . "', correo = '" . $this -> correo . "', estado = '" . $this -> estado . "', idCarrera = '" . $this -> carrera. "'
                where idProfesor = '" . $this -> idProfesor .  "'";
    }
    
    public function editar_clave(){
        return "update profesor
                set clave = '" . md5($this -> clave) . "'
                where idProfesor = '" . $this -> idProfesor .  "'";
    }

    public function cambiarEstado($estado){
        return "update profesor
                set estado = '" . $estado . "'
                where idProfesor = '" . $this -> idProfesor .  "'";
    }

    public function consultarCarrera(){
        return "select idCarrera
                from profesor
                where idProfesor = '" . $this -> idProfesor .  "'";
    }
    
}

?>
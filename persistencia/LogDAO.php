<?php
class logDAO{
    private $idLog;
    private $accion;
    private $datos;
    private $fecha;
    private $hora;
    private $actor;

    public function LogDAO($idLog = "", $accion="", $datos = "", $fecha ="", $hora ="", $actor){
        $this -> idLog = $idLog;
        $this -> accion = $accion;
        $this -> datos = $datos;
        $this -> fecha = $fecha;
        $this -> hora = $hora;
        $this -> actor = $actor;
    }

    public function consultarPaginacion($cantidad, $pagina){
        return "select idLog, accion, datos, fecha, hora, actor
                from Log
                limit " . (($pagina-1) * $cantidad) . ", " . $cantidad;
    }

    public function consultarCantidad(){
        return "select count(idLog)
                from Log";
    }

    public function insertar(){
        return "insert into Log (accion, datos, fecha, hora, actor)
                values ('" . $this -> accion . "', '" . $this -> datos . "', '" . $this -> fecha . "', '" . $this -> hora . "',  '" . $this -> actor . "')";
    }
}

<?php 
class CasoDAO{
    private $idCaso;
    private $fecha;
    private $estado;
    private $aspirante;
    private $carrera;
    private $pin;

    public function CasoDAO($idCaso = "", $fecha = "", $estado= "", $aspirante="", $carrera="", $pin=""){
        $this -> idCaso = $idCaso;
        $this -> fecha = $fecha;
        $this -> estado = $estado;
        $this -> aspirante = $aspirante;
        $this -> carrera = $carrera;
        $this -> pin = $pin;
    }

    public function insertar(){
        return "insert into caso (fecha, estado, idAspirante, idCarrera, idPin)
                values ('" . $this -> fecha . "', '" . $this -> estado . "', " . $this -> aspirante . ", null, ".$this -> pin.")";
    } 

    public function consultarTodos(){
        return "select idCaso, fecha, estado, idAspirante, idCarrera, idPin
                from caso";
    }

    public function consultar(){
        return "select idCaso, fecha, estado, idAspirante, idCarrera, idPin
                from caso
                where idAspirante = ". $this -> aspirante ."
                ";
    }

    public function consultar2(){
        return "select c.fecha, c.estado, a.nombre, a.apellido, c.idCarrera, c.idPin
                from caso c, aspirante a 
                where a.idAspirante = c.idAspirante and idCaso = ". $this -> idCaso ."
                ";
    }

    public function consultar3(){
        return "select fecha, estado,  idAspirante, idCarrera, idPin
                from caso
                where idCaso = ". $this -> idCaso ."
                ";
    }

    public function modificarEstadoCarrera($estado, $carrera){
        return "update caso
                set estado = '".$estado."', idCarrera = '".$carrera."'
                where idAspirante = '".$this -> aspirante."'";
    }
  
    public function modificarEstadoFecha($estado, $fecha){
        return "update caso
                set estado = '".$estado."', fecha = '".$fecha."'
                where idAspirante = '".$this -> aspirante."'";
    }

    public function modificarEstadoFechaCaso($estado, $fecha){
        return "update caso
                set estado = '".$estado."', fecha = '".$fecha."'
                where idCaso = '".$this -> idCaso."'";
    }

    public function consultarPaginacion($cantidad, $pagina, $filtro){
        return "select c.idCaso, c.fecha, c.estado, a.nombre, a.apellido
                from caso c, aspirante a
                where c.idAspirante = a.idAspirante and c.estado like '%".$filtro."' and c.idCarrera = '".$this -> carrera."'
                limit " . (($pagina-1) * $cantidad) . ", " . $cantidad;
    }
    
    public function consultarCantidad(){
        return "select count(idCaso)
                from caso
                where idCarrera = '".$this -> carrera."'";
    }

    public function consultarCantidadAdmitidos($filtro, $filtro2){
        return "select count(idCaso)
                from caso
                where estado like '%".$filtro."' and idCarrera like '%".$filtro2."'";
    }
}

?>
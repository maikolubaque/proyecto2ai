<?php
require_once "persistencia/Conexion.php";
require_once "persistencia/Entrevista_PreguntaDAO.php";
class Entrevista_Pregunta{
    private $idEntrevista_Pregunta;
    private $pregunta;
    private $respuesta;
    private $puntaje;
    private $entrevista;
    private $conexion;
    private $Entrevista_PreguntaDAO;
    
    public function getIdEntrevista_Pregunta(){
        return $this -> idEntrevista_Pregunta;
    }
    
    public function getRespuesta(){
        return $this -> respuesta;
    }

    public function getPregunta(){
        return $this -> pregunta;
    }
    
    public function getPuntaje(){
        return $this -> puntaje;
    }

    public function getEntrevista(){
        return $this -> entrevista;
    }


    
    public function Entrevista_Pregunta($idEntrevista_Pregunta="" ,$pregunta = "", $respuesta = "",  $puntaje = "",  $entrevista = ""){
        $this -> idEntrevista_Pregunta = $idEntrevista_Pregunta;
        $this -> pregunta = $pregunta;
        $this -> respuesta = $respuesta;
        $this -> puntaje = $puntaje;
        $this -> entrevista = $entrevista;
        $this -> conexion = new Conexion();
        $this -> Entrevista_PreguntaDAO = new Entrevista_PreguntaDAO($this -> idEntrevista_Pregunta, $this -> pregunta, $this -> respuesta,  $this -> puntaje,   $this -> entrevista);
    }

    
    public function insertar(){
        $this -> conexion -> abrir();        
        $this -> conexion -> ejecutar($this -> Entrevista_PreguntaDAO -> insertar()); 
        $this -> conexion -> cerrar();        
    }
    
    public function consultarTodos(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> Entrevista_PreguntaDAO -> consultarTodos());
        $Entrevista_Preguntas = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $p = new Entrevista_Pregunta($resultado[0], $resultado[1], $resultado[2], $resultado[3]);
            array_push($Entrevista_Preguntas, $p);
        }
        $this -> conexion -> cerrar();        
        return $Entrevista_Preguntas;
    }

    public function consultarRespusta($caso){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> Entrevista_PreguntaDAO -> consultarRespuesta($caso));
        $Entrevista_Preguntas = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $p = new Entrevista_Pregunta($resultado[0],$resultado[1], $resultado[2], $resultado[3], $resultado[4]);
            array_push($Entrevista_Preguntas, $p);
        }
        $this -> conexion -> cerrar();        
        return $Entrevista_Preguntas;
    }
    
    public function editar($nota){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> Entrevista_PreguntaDAO -> editar($nota));
        $this -> conexion -> cerrar();
    }
    
}

?>

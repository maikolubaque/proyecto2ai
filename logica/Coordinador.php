<?php
require_once "persistencia/Conexion.php";
require_once "persistencia/CoordinadorDAO.php";

class Coordinador{
    private $idCoordinador;
    private $nombre;
    private $apellido;
    private $correo;
    private $clave;
    private $carrera;
    private $estado;      
    private $conexion;
    private $CoordinadorDAO;

    public function getIdCoordinador(){
        return $this -> idCoordinador;
    }

    public function getNombre(){
        return $this -> nombre;
    }

    public function getApellido(){
        return $this -> apellido;
    }

    public function getCorreo(){
        return $this -> correo;
    }

    public function getClave(){
        return $this -> clave;
    }

    public function getCarrera(){
        return $this -> carrera;
    }

    public function getEstado(){
        return $this -> estado;
    }
    
    public function Coordinador($idCoordinador= "", $nombre = "", $apellido = "", $correo = "", $clave = "", $carrera = "", $estado = ""){
        $this -> idCoordinador = $idCoordinador;
        $this -> nombre = $nombre;
        $this -> apellido = $apellido;
        $this -> correo = $correo;
        $this -> clave = $clave;
        $this -> carrera = $carrera;
        $this -> estado = $estado;
        $this -> conexion = new Conexion();
        $this -> CoordinadorDAO = new CoordinadorDAO($this -> idCoordinador, $this -> nombre, $this -> apellido, $this -> correo, $this -> clave, $this -> carrera, $this -> estado);
    }
   
    public function existeCorreo(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> CoordinadorDAO -> existeCorreo());
        $this -> conexion -> cerrar();
        return $this -> conexion -> numFilas();
    }
    
    
    public function registrar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> CoordinadorDAO -> registrar());
        $this -> conexion -> cerrar();
    }
   
    
    public function autenticar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> CoordinadorDAO -> autenticar());

        $this -> conexion -> cerrar();
        if ($this -> conexion -> numFilas() == 1){
            $resultado = $this -> conexion -> extraer();
            $this -> idCoordinador = $resultado[0];
            $this -> estado = $resultado[1];
            $this -> nombre = $resultado[2];
            $this -> apellido = $resultado[3];
            return true;
        }else {
            return false;
        }
    }
    
    public function consultar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> CoordinadorDAO -> consultar());
        $this -> conexion -> cerrar();
        $resultado = $this -> conexion -> extraer();
        $this -> nombre = $resultado[0];
        $this -> apellido = $resultado[1];
        $this -> correo = $resultado[2];
        $this -> clave = $resultado[3];
        $this -> estado = $resultado[5];
        $this -> carrera = $resultado[6];
        
    }
    
    public function consultarPaginacion($cantidad, $pagina){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> CoordinadorDAO -> consultarPaginacion($cantidad, $pagina));
        $coordinadores = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $e = new Coordinador($resultado[0], $resultado[1], $resultado[2],$resultado[3],"",$resultado[5], $resultado[4]);
            array_push($coordinadores, $e);
        }
        $this -> conexion -> cerrar();
        return $coordinadores;
    }
    
    public function consultarCantidad(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this ->CoordinadorDAO -> consultarCantidad());
        $this -> conexion -> cerrar();
        return $this -> conexion -> extraer()[0];
    }
    
    public function editar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> CoordinadorDAO -> editar());
        $this -> conexion -> cerrar();
    }
    
    public function editar_clave(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> CoordinadorDAO-> editar_clave());
        $this -> conexion -> cerrar();
    }

    public function cambiarEstado($estado){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> CoordinadorDAO -> cambiarEstado($estado));
        $this -> conexion -> cerrar();
    }

    public function consultarCarrera(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> CoordinadorDAO -> consultarCarrera());
        $this -> conexion -> cerrar();
        return $this -> conexion -> extraer()[0];
    }
}

?>
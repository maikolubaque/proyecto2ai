<?php
require_once "persistencia/Conexion.php";
require_once "persistencia/AcademiaDAO.php";

class Academia{
    private $idAcademia;
    private $colegio;
    private $fechaGrado;
    private $icfes;
    private $certificado;
    private $aspirante;
    private $fechaCargue;   
    private $conexion;
    private $academiaDAO;

    public function getIdAcademia(){
        return $this -> idAcademia;
    }

    public function getColegio(){
        return $this -> colegio;
    }

    public function getFechaGrado(){
        return $this -> fechaGrado;
    }

    public function getIcfes(){
        return $this -> icfes;
    }

    public function getCertificado(){
        return $this -> certificado;
    }

    public function getAspirante(){
        return $this -> aspirante;
    }

    public function getFechaCargue(){
        return $this -> fechaCargue;
    }


    
    public function Academia($idAcademia= "", $colegio = "", $fechaGrado = "", $icfes = "", $certificado = "", $aspirante = "", $fechaCargue = ""){
        $this -> idAcademia = $idAcademia;
        $this -> colegio = $colegio;
        $this -> fechaGrado = $fechaGrado;
        $this -> icfes = $icfes;
        $this -> certificado = $certificado;
        $this -> aspirante = $aspirante;
        $this -> fechaCargue = $fechaCargue;
        $this -> conexion = new Conexion();
        $this -> academiaDAO = new academiaDAO($this -> idAcademia, $this -> colegio, $this -> fechaGrado, $this -> icfes, $this -> certificado, $this -> aspirante, $this -> fechaCargue);
    }
   
    
    public function registrar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> academiaDAO -> registrar());
        echo $this -> academiaDAO -> registrar();
        $this -> conexion -> cerrar();
    }
   

    
    public function consultar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> academiaDAO -> consultar());
        $this -> conexion -> cerrar();
        $resultado = $this -> conexion -> extraer();
        $this -> colegio = $resultado[0];
        $this -> fechaGrado = $resultado[1];
        $this -> icfes = $resultado[2];
        $this -> certificado = $resultado[3];
        $this -> aspirante = $resultado[4];
        $this -> fechaCargue = $resultado[5];
    }
    

}

?>
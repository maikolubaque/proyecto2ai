<?php
require_once "persistencia/Conexion.php";
require_once "persistencia/EntrevistaDAO.php";

class Entrevista{
    private $idEntrevista;
    private $puntajeT;
    private $fecha;
    private $estado;
    private $profesor;
    private $caso;
    private $conexion;
    private $entrevistaDAO;
    
    public function getIdEntrevista(){
        return $this -> idEntrevista;
    }
    
    public function getPuntaje(){
        return $this -> puntajeT;
    }
   
    public function getFecha(){
        return $this->fecha;
    }


    public function getestado(){
        return $this->estado;
    }


    public function getProfesor(){
        return $this->profesor;
    }

    public function getCaso(){
        return $this->caso;
    }
        
    public function Entrevista($idEntrevista = "", $puntajeT = "", $fecha= "", $estado="", $profesor="", $caso=""){
        $this -> idEntrevista = $idEntrevista;
        $this -> puntajeT = $puntajeT;
        $this -> fecha = $fecha;
        $this -> estado = $estado;
        $this -> profesor = $profesor;
        $this -> caso = $caso;
        $this -> conexion = new Conexion();
        $this -> entrevistaDAO = new entrevistaDAO($this -> idEntrevista, $this -> puntajeT, $this -> fecha, $this -> estado, $this -> profesor, $this -> caso);
    }

    public function consultar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> entrevistaDAO -> consultar());
        $this -> conexion -> cerrar();
        $resultado = $this -> conexion -> extraer();
        $this -> idEntrevista = $resultado[0];
        $this -> puntajeT = $resultado[1];
        $this -> fecha = $resultado[2];
        $this -> estado = $resultado[3];
        $this -> profesor = $resultado[4];
        $this -> caso = $resultado[5];
   
    }

    public function consultar2(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> entrevistaDAO -> consultar2());
        $this -> conexion -> cerrar();
        $resultado = $this -> conexion -> extraer();
        $this -> idEntrevista = $resultado[0];
        $this -> puntajeT = $resultado[1];
        $this -> fecha = $resultado[2];
        $this -> estado = $resultado[3];
        $this -> profesor = $resultado[4];
   
    }

    public function consultarPaginacion($cantidad, $pagina){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> entrevistaDAO -> consultarPaginacion($cantidad, $pagina));
        $entrevistas = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $e = new Entrevista($resultado[0], $resultado[1], $resultado[2],$resultado[3], $resultado[4], $resultado[5]);
            array_push($entrevistas, $e);
        }
        $this -> conexion -> cerrar();
        return $entrevistas;
    }

    public function consultarCantidad(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this ->entrevistaDAO -> consultarCantidad());
        $this -> conexion -> cerrar();
        return $this -> conexion -> extraer()[0];
    }
    
    public function registrar(){
        $this -> conexion -> abrir();        
        $this -> conexion -> ejecutar($this -> entrevistaDAO -> registrar());  
        $this -> conexion -> ejecutar($this -> entrevistaDAO -> consultarId()); 
        $this -> conexion -> cerrar();   
        $resultado = $this -> conexion -> extraer();
        return $resultado[0]; 
    }
    
    public function editar(){
        $this -> conexion -> abrir();        
        $this -> conexion -> ejecutar($this -> entrevistaDAO -> editar());  
        $this -> conexion -> cerrar();   
    }


 
}

?>
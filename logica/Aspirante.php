<?php
require_once "persistencia/Conexion.php";
require_once "persistencia/AspiranteDAO.php";

class Aspirante{
    private $idAspirante;
    private $nombre;
    private $apellido;
    private $cedula;
    private $sexo;
    private $telefono;
    private $correo;
    private $clave;
    private $carrera;
    private $estado;      
    private $conexion;
    private $aspiranteDAO;

    public function getIdAspirante(){
        return $this -> idAspirante;
    }

    public function getNombre(){
        return $this -> nombre;
    }

    public function getApellido(){
        return $this -> apellido;
    }

    public function getCedula(){
        return $this -> cedula;
    }

    public function getSexo(){
        return $this -> sexo;
    }

    public function getTelefono(){
        return $this -> telefono;
    }

    public function getCorreo(){
        return $this -> correo;
    }

    public function getClave(){
        return $this -> clave;
    }

    public function getCarrera(){
        return $this -> carrera;
    }

    public function getEstado(){
        return $this -> estado;
    }
    
    public function Aspirante($idAspirante= "", $nombre = "", $apellido = "", $cedula = "", $sexo = "", $telefono = "", $correo = "", $clave = "", $carrera = "", $estado = ""){
        $this -> idAspirante = $idAspirante;
        $this -> nombre = $nombre;
        $this -> apellido = $apellido;
        $this -> cedula = $cedula;
        $this -> sexo = $sexo;
        $this -> telefono = $telefono;
        $this -> correo = $correo;
        $this -> clave = $clave;
        $this -> carrera = $carrera;
        $this -> estado = $estado;
        $this -> conexion = new Conexion();
        $this -> aspiranteDAO = new aspiranteDAO($this -> idAspirante, $this -> nombre, $this -> apellido, $this -> cedula, $this -> sexo, $this -> telefono, $this -> correo, $this -> clave, $this -> carrera, $this -> estado);
    }
   
    public function existeCorreo(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> aspiranteDAO -> existeCorreo());
        $this -> conexion -> cerrar();
        return $this -> conexion -> numFilas();
    }
    
    
    public function registrar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> aspiranteDAO -> registrar());
        $this -> conexion -> cerrar();
    }
   
    
    public function autenticar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> aspiranteDAO -> autenticar());
        $this -> conexion -> cerrar();
        if ($this -> conexion -> numFilas() == 1){
            $resultado = $this -> conexion -> extraer();
            $this -> idAspirante = $resultado[0];
            $this -> estado = $resultado[1];
            $this -> nombre = $resultado[2];
            $this -> apellido = $resultado[3];
            return true;
        }else {
            return false;
        }
    }
    
    public function consultar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> aspiranteDAO -> consultar());
        $this -> conexion -> cerrar();
        $resultado = $this -> conexion -> extraer();
        $this -> nombre = $resultado[0];
        $this -> apellido = $resultado[1];
        $this -> cedula = $resultado[2];
        $this -> sexo = $resultado[3];
        $this -> telefono = $resultado[4];
        $this -> correo = $resultado[5];
        
    }
    
    public function consultarPaginacion($cantidad, $pagina){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> aspiranteDAO -> consultarPaginacion($cantidad, $pagina));
        $Aspirantees = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $e = new Aspirante($resultado[0], $resultado[1], $resultado[2],$resultado[3],"",$resultado[4], $resultado[5]);
            array_push($Aspirantees, $e);
        }
        $this -> conexion -> cerrar();
        return $Aspirantees;
    }
    
    public function consultarCantidad(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this ->aspiranteDAO -> consultarCantidad());
        $this -> conexion -> cerrar();
        return $this -> conexion -> extraer()[0];
    }
    
    public function editar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> aspiranteDAO -> editar());
        $this -> conexion -> cerrar();
    }
    
    public function editar_clave(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> aspiranteDAO-> editar_clave());
        $this -> conexion -> cerrar();
    }

    public function cambiarEstado($estado){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> aspiranteDAO -> cambiarEstado($estado));
        $this -> conexion -> cerrar();
    }

    public function consultarCarrera(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> aspiranteDAO -> consultarCarrera());
        $this -> conexion -> cerrar();
        return $this -> conexion -> extraer()[0];
    }
}

?>
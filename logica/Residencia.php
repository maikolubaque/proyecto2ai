<?php
require_once "persistencia/Conexion.php";
require_once "persistencia/ResidenciaDAO.php";

class Residencia{
    private $idResidencia;
    private $estrato;
    private $direccion;
    private $localidad;
    private $ciudad;
    private $certificado;
    private $aspirante;
    private $consteoOtro;
    private $ganancias;    
    private $conexion;
    private $residenciaDAO;

    public function getIdResidencia(){
        return $this -> idResidencia;
    }

    public function getEstrato(){
        return $this -> estrato;
    }

    public function getDireccion(){
        return $this -> direccion;
    }

    public function getLocalidad(){
        return $this -> localidad;
    }

    public function getCiudad(){
        return $this -> ciudad;
    }

    public function getCertificado(){
        return $this -> certificado;
    }

    public function getAspirante(){
        return $this -> aspirante;
    }

    public function getCosteoOtro(){
        return $this -> consteoOtro;
    }

    public function getGanancias(){
        return $this -> ganancias;
    }


    
    public function Residencia($idResidencia= "", $estrato = "", $direccion = "", $localidad = "", $ciudad = "", $certificado = "", $aspirante = "", $consteoOtro = "", $ganancias = ""){
        $this -> idResidencia = $idResidencia;
        $this -> estrato = $estrato;
        $this -> direccion = $direccion;
        $this -> localidad = $localidad;
        $this -> ciudad = $ciudad;
        $this -> certificado = $certificado;
        $this -> aspirante = $aspirante;
        $this -> consteoOtro = $consteoOtro;
        $this -> ganancias = $ganancias;
        $this -> conexion = new Conexion();
        $this -> residenciaDAO = new residenciaDAO($this -> idResidencia, $this -> estrato, $this -> direccion, $this -> localidad, $this -> ciudad, $this -> certificado, $this -> aspirante, $this -> consteoOtro, $this -> ganancias);
    }
   
    
    public function registrar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> residenciaDAO -> registrar());
        $this -> conexion -> cerrar();
    }
   

    
    public function consultar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> residenciaDAO -> consultar());
        $this -> conexion -> cerrar();
        $resultado = $this -> conexion -> extraer();
        $this -> estrato = $resultado[0];
        $this -> direccion = $resultado[1];
        $this -> localidad = $resultado[2];
        $this -> ciudad = $resultado[3];
        $this -> certificado = $resultado[4];
        $this -> aspirante = $resultado[5];
        $this -> consteoOtro = $resultado[6];
        $this -> ganancias = $resultado[7];
        
    }
    
    public function consultarPaginacion($cantidad, $pagina){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> residenciaDAO -> consultarPaginacion($cantidad, $pagina));
        $Residenciaes = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $e = new Residencia($resultado[0], $resultado[1], $resultado[2],$resultado[3],$resultado[4], $resultado[5], $resultado[6], $resultado[7]);
            array_push($Residenciaes, $e);
        }
        $this -> conexion -> cerrar();
        return $Residenciaes;
    }

}

?>
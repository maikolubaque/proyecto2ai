<?php
require_once "persistencia/Conexion.php";
require_once "persistencia/CasoDAO.php";

class Caso{
    private $idCaso;
    private $fecha;
    private $estado;
    private $aspirante;
    private $carrera;
    private $pin;
    private $conexion;
    private $casoDAO;
    
    public function getIdCaso(){
        return $this -> idCaso;
    }

    public function getFecha(){
        return $this->fecha;
    }
    
    public function getEstado(){
        return $this -> estado;
    }

    public function getAspirante(){
        return $this->aspirante;
    }
   
    public function getCarrera(){
        return $this->carrera;
    }

    public function getPin(){
        return $this->pin;
    }


    public function setEstado($estado){
        $this->estado = $estado;
    }
        
    public function Caso($idCaso = "", $fecha = "", $estado= "", $aspirante="", $carrera="", $pin=""){
        $this -> idCaso = $idCaso;
        $this -> fecha = $fecha;
        $this -> estado = $estado;
        $this -> aspirante = $aspirante;
        $this -> carrera = $carrera;
        $this -> pin = $pin;
        $this -> conexion = new Conexion();
        $this -> casoDAO = new CasoDAO($this -> idCaso, $this -> fecha, $this -> estado, $this -> aspirante, $this -> carrera, $this -> pin);
    }

    public function consultar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> casoDAO -> consultar());
        $this -> conexion -> cerrar();
        $resultado = $this -> conexion -> extraer();
        if($this -> conexion -> numFilas() >= 1 ){
            $this -> idCaso = $resultado[0];
            $this -> fecha = $resultado[1];
            $this -> estado = $resultado[2];
            $this -> aspirante = $resultado[3];
            $this -> carrera = $resultado[4];
            $this -> pin = $resultado[5];
        }
    }

    public function consultar2(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> casoDAO -> consultar2());
        $this -> conexion -> cerrar();
        $resultado = $this -> conexion -> extraer();
        if($this -> conexion -> numFilas() >= 1 ){
            $this -> fecha = $resultado[0];
            $this -> estado = $resultado[1];
            $this -> aspirante = $resultado[2]." ".$resultado[3];
            $this -> carrera = $resultado[4];
            $this -> pin = $resultado[5];
        }
    }

    public function consultar3(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> casoDAO -> consultar3());
        $this -> conexion -> cerrar();
        $resultado = $this -> conexion -> extraer();
        if($this -> conexion -> numFilas() >= 1 ){
            $this -> fecha = $resultado[0];
            $this -> estado = $resultado[1];
            $this -> aspirante = $resultado[2];
            $this -> carrera = $resultado[3];
            $this -> pin = $resultado[4];
        }
    }

    public function consultarTodos(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> casoDAO -> consultarTodos());
        $casos = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $t = new Caso($resultado[0], $resultado[1]);
            array_push($casos, $t);
        }
        $this -> conexion -> cerrar();        
        return $casos;
    }
    
    public function insertar(){
        $this -> conexion -> abrir();        
        $this -> conexion -> ejecutar($this -> casoDAO -> insertar());  
        $this -> conexion -> cerrar();  
    }

    public function modificarEstadoCarrera($estado, $carrera){
        $this -> conexion -> abrir();        
        $this -> conexion -> ejecutar($this -> casoDAO -> modificarEstadoCarrera($estado, $carrera)); 
        $this -> conexion -> cerrar();  
    }

    public function modificarEstadoFecha($estado, $fecha){
        $this -> conexion -> abrir();        
        $this -> conexion -> ejecutar($this -> casoDAO -> modificarEstadoFecha($estado, $fecha)); 
        $this -> conexion -> cerrar();  
    }

    public function modificarEstadoFechaCaso($estado, $fecha){
        $this -> conexion -> abrir();        
        $this -> conexion -> ejecutar($this -> casoDAO -> modificarEstadoFechaCaso($estado, $fecha)); 
        $this -> conexion -> cerrar();  
    }

    public function consultarPaginacion($cantidad, $pagina, $filtro){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> casoDAO -> consultarPaginacion($cantidad, $pagina, $filtro));
        $casos = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $e = new Caso($resultado[0], $resultado[1], $resultado[2],$resultado[3]." ".$resultado[4]);
            array_push($casos, $e);
        }
        $this -> conexion -> cerrar();
        return $casos;
    }
    
    public function consultarCantidad(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> casoDAO -> consultarCantidad());
        $this -> conexion -> cerrar();
        return $this -> conexion -> extraer()[0];
    }

    public function consultarCantidadAdmitidos($filtro, $filtro2=""){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> casoDAO -> consultarCantidadAdmitidos($filtro, $filtro2));
        $this -> conexion -> cerrar();
        return $this -> conexion -> extraer()[0];
    }
    
}

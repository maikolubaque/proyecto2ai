<?php
require_once "persistencia/Conexion.php";
require_once "persistencia/PinDAO.php";

class Pin{
    private $idPin;
    private $precio;
    private $banco;
    private $fecha;
    private $cedula;
    private $conexion;
    private $pinDAO;
    
    public function getIdPin(){
        return $this -> idPin;
    }
    
    public function getPrecio(){
        return $this -> precio;
    }
   
    public function getBanco(){
        return $this->banco;
    }


    public function getFecha(){
        return $this->fecha;
    }


    public function getCedula(){
        return $this->cedula;
    }
        
    public function Pin($idPin = "", $precio = "", $banco= "", $fecha="", $cedula=""){
        $this -> idPin = $idPin;
        $this -> precio = $precio;
        $this -> banco = $banco;
        $this -> fecha = $fecha;
        $this -> cedula = $cedula;
        $this -> conexion = new Conexion();
        $this -> pinDAO = new PinDAO($this -> idPin, $this -> precio, $this -> banco, $this -> fecha, $this -> cedula);
    }

    public function consultar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> pinDAO -> consultar());
        $this -> conexion -> cerrar();
        $resultado = $this -> conexion -> extraer();
        $this -> precio = $resultado[0];
        $this -> banco = $resultado[1];
        $this -> fecha = $resultado[2];
        $this -> cedula = $resultado[3];
   
    }

    public function consultarTodos(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> pinDAO -> consultarTodos());
        $pins = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $t = new Pin($resultado[0], $resultado[1]);
            array_push($pins, $t);
        }
        $this -> conexion -> cerrar();        
        return $pins;
    }
    
    public function insertar(){
        $this -> conexion -> abrir();        
        $this -> conexion -> ejecutar($this -> pinDAO -> insertar());  
        $this -> conexion -> ejecutar($this -> pinDAO -> consultarId()); 
        $this -> conexion -> cerrar();   
        $resultado = $this -> conexion -> extraer();
        return $resultado[0]; 
    }
    



 
}

?>
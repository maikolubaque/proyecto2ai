<?php
require_once "persistencia/Conexion.php";
require_once "persistencia/PreguntaDAO.php";

class Pregunta{
    private $idPregunta;
    private $pregunta;
    private $carrera;
    private $conexion;
    private $preguntaDAO;
    
    public function getIdPregunta(){
        return $this -> idPregunta;
    }
    
    public function getPregunta(){
        return $this -> pregunta;
    }
   
    public function getCarrera(){
        return $this->carrera;
    }
        
    public function Pregunta($idPregunta = "", $pregunta = "", $carrera){
        $this -> idPregunta = $idPregunta;
        $this -> pregunta = $pregunta;
        $this -> carrera = $carrera;
        $this -> conexion = new Conexion();
        $this -> preguntaDAO = new PreguntaDAO($this -> idPregunta, $this -> pregunta, $this -> carrera);
    }

    public function consultar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> preguntaDAO -> consultar());
        $this -> conexion -> cerrar();
        $resultado = $this -> conexion -> extraer();
        $this -> idPregunta = $resultado[0];
        $this -> pregunta = $resultado[1];
        $this -> pregunta = $resultado[2];
   
    }

    public function consultarTodos(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> preguntaDAO -> consultarTodos());
        $preguntas = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $t = new Pregunta($resultado[0], $resultado[1], $resultado[2]);
            array_push($preguntas, $t);
        }
        $this -> conexion -> cerrar();        
        return $preguntas;
    }
    
    public function insertar(){
        $this -> conexion -> abrir();        
        $this -> conexion -> ejecutar($this -> preguntaDAO -> insertar());  
        $this -> conexion -> cerrar();    
    }
    
    public function eliminar(){
        $this -> conexion -> abrir();        
        $this -> conexion -> ejecutar($this -> preguntaDAO -> eliminar());  
        $this -> conexion -> cerrar();    
    }


    
    
}

?>
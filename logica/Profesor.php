<?php
require_once "persistencia/Conexion.php";
require_once "persistencia/ProfesorDAO.php";

class Profesor{
    private $idProfesor;
    private $nombre;
    private $apellido;
    private $correo;
    private $clave;
    private $carrera;
    private $estado;      
    private $conexion;
    private $profesorDAO;

    public function getIdProfesor(){
        return $this -> idProfesor;
    }

    public function getNombre(){
        return $this -> nombre;
    }

    public function getApellido(){
        return $this -> apellido;
    }

    public function getCorreo(){
        return $this -> correo;
    }

    public function getClave(){
        return $this -> clave;
    }

    public function getCarrera(){
        return $this -> carrera;
    }

    public function getEstado(){
        return $this -> estado;
    }
    
    public function Profesor($idProfesor= "", $nombre = "", $apellido = "", $correo = "", $clave = "", $carrera = "", $estado = ""){
        $this -> idProfesor = $idProfesor;
        $this -> nombre = $nombre;
        $this -> apellido = $apellido;
        $this -> correo = $correo;
        $this -> clave = $clave;
        $this -> carrera = $carrera;
        $this -> estado = $estado;
        $this -> conexion = new Conexion();
        $this -> profesorDAO = new profesorDAO($this -> idProfesor, $this -> nombre, $this -> apellido, $this -> correo, $this -> clave, $this -> carrera, $this -> estado);
    }
   
    public function existeCorreo(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> profesorDAO -> existeCorreo());
        $this -> conexion -> cerrar();
        return $this -> conexion -> numFilas();
    }
    
    
    public function registrar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> profesorDAO -> registrar());
        $this -> conexion -> cerrar();
    }
   
    
    public function autenticar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> profesorDAO -> autenticar());
        $this -> conexion -> cerrar();
        if ($this -> conexion -> numFilas() == 1){
            $resultado = $this -> conexion -> extraer();
            $this -> idProfesor = $resultado[0];
            $this -> estado = $resultado[1];
            $this -> nombre = $resultado[2];
            $this -> apellido = $resultado[3];
            return true;
        }else {
            return false;
        }
    }
    
    public function consultar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> profesorDAO -> consultar());
        $this -> conexion -> cerrar();
        $resultado = $this -> conexion -> extraer();
        $this -> nombre = $resultado[0];
        $this -> apellido = $resultado[1];
        $this -> correo = $resultado[2];
        $this -> clave = $resultado[3];
        $this -> carrera = $resultado[4];
        $this -> estado = $resultado[5];
        
    }
    
    public function consultarPaginacion($cantidad, $pagina){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> profesorDAO -> consultarPaginacion($cantidad, $pagina));
        $Profesores = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $e = new Profesor($resultado[0], $resultado[1], $resultado[2],$resultado[3],"",$resultado[4], $resultado[5]);
            array_push($Profesores, $e);
        }
        $this -> conexion -> cerrar();
        return $Profesores;
    }
    
    public function consultarCantidad(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this ->profesorDAO -> consultarCantidad());
        $this -> conexion -> cerrar();
        return $this -> conexion -> extraer()[0];
    }
    
    public function editar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> profesorDAO -> editar());
        $this -> conexion -> cerrar();
    }
    
    public function editar_clave(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> profesorDAO-> editar_clave());
        $this -> conexion -> cerrar();
    }

    public function cambiarEstado($estado){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> profesorDAO -> cambiarEstado($estado));
        $this -> conexion -> cerrar();
    }

    public function consultarCarrera(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> profesorDAO -> consultarCarrera());
        $this -> conexion -> cerrar();
        return $this -> conexion -> extraer()[0];
    }
}

?>
<?php
require_once "persistencia/Conexion.php";
require_once "persistencia/CarreraDAO.php";

class Carrera{
    private $idCarrera;
    private $carrera;
    private $conexion;
    private $carreraDAO;
    
    public function getIdCarrera(){
        return $this -> idCarrera;
    }
    
    public function getCarrera(){
        return $this -> carrera;
    }
   
        
    public function Carrera($idCarrera = "", $carrera = ""){
        $this -> idCarrera = $idCarrera;
        $this -> carrera = $carrera;
        $this -> conexion = new Conexion();
        $this -> carreraDAO = new CarreraDAO($this -> idCarrera, $this -> carrera);
    }

    public function consultar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> carreraDAO -> consultar());
        $this -> conexion -> cerrar();
        $resultado = $this -> conexion -> extraer();
        $this -> idCarrera = $resultado[0];
        $this -> carrera = $resultado[1];
   
    }

    public function consultarTodos(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> carreraDAO -> consultarTodos());
        $carreras = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $t = new Carrera($resultado[0], $resultado[1]);
            array_push($carreras, $t);
        }
        $this -> conexion -> cerrar();        
        return $carreras;
    }
    
    public function insertar(){
        $this -> conexion -> abrir();        
        $this -> conexion -> ejecutar($this -> carreraDAO -> insertar());        
        $this -> conexion -> cerrar();    
    }
    

}

?>
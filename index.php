<?php 
session_start();
require_once "logica/Administrador.php";
require_once "logica/Profesor.php";
require_once "logica/Coordinador.php";
require_once "logica/Carrera.php";
require_once "logica/Pregunta.php";
require_once "logica/Aspirante.php";
require_once "logica/Pin.php";
require_once "logica/Caso.php";
require_once "logica/Residencia.php";
require_once "logica/Entrevista.php";
require_once "logica/Entrevista_Pregunta.php";
require_once "logica/Academia.php";
require_once "logica/Log.php";
date_default_timezone_set('America/Bogota');

$pid = "";
if(isset($_GET["pid"])){
    $pid = base64_decode($_GET["pid"]);    
}else{
    $_SESSION["id"]="";
    $_SESSION["rol"]="";
}
if(isset($_GET["cerrarSesion"]) || !isset($_SESSION["id"])){
    $_SESSION["id"]="";
}
?>
<html>
<head>
	<link rel="icon" type="image/png" href="img/logo.png" />
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" >
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.1/css/all.css" />
	<script src="https://code.jquery.com/jquery-3.4.	1.min.js" ></script>
	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" ></script>	
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" ></script>
	<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
	<script>
	$(function () {
		  $('[data-toggle="tooltip"]').tooltip()
	})
	</script>
</head>
<body>
	<?php 
	$paginasSinSesion = array(
        "presentacion/autenticar.php",
	    "presentacion/aspirante/registrarAspirante.php",
	    "presentacion/cliente/activarCliente.php"
	);	
	if(in_array($pid, $paginasSinSesion)){
		include "presentacion/head.php";
	    include $pid;
	}else if($_SESSION["id"]!="") {
	    if($_SESSION["rol"] == "Administrador"){
	        include "presentacion/administrador/menuAdministrador.php";
	    }else if($_SESSION["rol"] == "Profesor"){
	        include "presentacion/profesor/menuProfesor.php";
	    }else if($_SESSION["rol"] == "Coordinador"){
	        include "presentacion/coordinador/menuCoordinador.php";
	    }	else if($_SESSION["rol"] == "Aspirante")	{
			include "presentacion/aspirante/menuAspirante.php";
		}    
	    include $pid;
	}else{
	    include "presentacion/head.php";
	    include "presentacion/inicio.php";
	}
	?>	
</body>
</html>